$('.header-menu').on('click', function(){
    if($('.menubar-container').find(".hidden").hasClass('open')) {
        $('.menubar-container').find(".hidden").removeClass('open');
        $('#icon-hamburger').show();
        $('#icon-close').hide();
        
    } else {
        $('.menubar-container').find(".hidden").addClass('open');
        $('#icon-close').show();
        $('#icon-hamburger').hide();
    }
    

})

$('.subscribe-button').on('click', function(){
	$('#modal-host').show();
})
$('.backdrop').on('click', function(){
	$('#modal-host').hide();
})
$('.window-close-button').on('click', function(){
	$('#modal-host').hide();
})	
