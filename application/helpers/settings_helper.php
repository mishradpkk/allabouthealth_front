<?php 

function meta(){
    $meta = new \stdClass();
    $project_name = 'TheViralStreet';
    
    $meta->logo_url = base_url() . 'assets/images/logo.png';
    $meta->project_banner = base_url() . 'assets/images/cover.jpg';
    $meta->favicon_url = base_url() . 'assets/images/favicon.png';
    
    $meta->project_name = 'TheViralStreet';
    $meta->project_title = 'TheViralStreet';
    $meta->project_url = 'theviralstreet.com';
    
    $meta->meta_title = 'TheViralStreet';
    $meta->meta_description = 'TheViralStreet offers the latest news and trending stories from India & the world featuring entertainment, humor, politics, fashion, lifestyle, sports to keep you updated!';
    $meta->meta_keywords = 'Bollywood, Politics, Sports, Social Media, viral, humor, lifestyle';
    
    $meta->logo_url = base_url() . 'assets/images/logo.png';
    $meta->project_banner = base_url() . 'assets/images/cover.jpg';
    $meta->favicon_url = base_url() . 'assets/images/favicon.png';
    $meta->theme_color = '#ffb17a';
    $meta->facebook_url = '';
    $meta->instagram_url = '';
    $meta->twitter_url = '';
    
    return $meta;
}
