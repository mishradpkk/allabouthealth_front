<?php 

function get_total_pages($query_condition, $limit=18){
    $CI =& get_instance();
    $CI->load->model('standard_model');
    
    $data_query['table'] = $query_condition['table'];
    $data_query['field'] = "count(distinct ".$query_condition['table'].".id) as total_ids";
    if(isset($query_condition['condition'])) {
        $data_query['condition'] = $query_condition['condition'];
    }
    if(isset($query_condition['join'])) {
        $data_query['join'] = $query_condition['join'];
    }        

    if(isset($query_condition['where_in'])) {
        $data_query['where_in'] = $query_condition['where_in'];
    }        

    if(isset($query_condition['or_where_in'])) {
        $data_query['or_where_in'] = $query_condition['or_where_in'];
    }        

    if(isset($query_condition['like'])) {
        $data_query['like'] = $query_condition['like'];
    }        
    
    $CI->standard_model->set_query_data($data_query);
    $result = $CI->standard_model->select();
    
    if(isset($result->total_ids)) {
        $total_pages = ceil($result->total_ids / $limit);
        return $total_pages;
    } else {
        return 0;
    }
} 

function get_next_story($article_id) {
    $CI =& get_instance();
    $CI->load->model('standard_model');
    $data_query['table']  = 'articles';
    $data_query['field']  = 'articles.id,title,articles.cover_image,articles.slug, date_of_action,categories.name as cat_name';
    $data_query['join']   = array(
        'categories' => 'articles.cat_id=categories.id',            
    );
  
    $data_query['condition'] = array(
        'articles.id <' => $article_id,
        'articles.status' => 'active',
    );

    $data_query['limit'] = 1;
    $data_query['order_by'] = array('articles.id' => 'DESC');
    $CI->standard_model->set_query_data($data_query);
    $result = $CI->standard_model->select();          


    if(!isset($result->id)) {
        $result = get_last_article();
    }
    
    return $result;     
}

function get_last_article() {
    $CI =& get_instance();
    $CI->load->model('standard_model');
    $data_query['table']  = 'articles';
    $data_query['field']  = 'articles.id,title,articles.cover_image,articles.slug, date_of_action,categories.name as cat_name';
    $data_query['join']   = array(
        'categories' => 'articles.cat_id=categories.id',            
    );
  
    $data_query['condition'] = array(
        // 'articles.id <' => $article_id,
        'articles.status' => 'active'
    );

    $data_query['limit'] = 1;
    $data_query['order_by'] = array('articles.id' => 'DESC');
    $CI->standard_model->set_query_data($data_query);
    $result = $CI->standard_model->select();   
    return $result;
}

function get_prev_story($article_id) {
    $CI =& get_instance();
    $CI->load->model('standard_model');
    $data_query['table']  = 'articles';         
    $data_query['field']  = 'articles.id,title,articles.cover_image,articles.slug, date_of_action,categories.name as cat_name';
  
    $data_query['join']   = array(
        'categories' => 'articles.cat_id=categories.id',            
    );

    $data_query['condition'] = array(
        'articles.id >' => $article_id,
        'articles.status' => 'active',
    );

    $data_query['limit'] = 1;
    $data_query['order_by'] = array('articles.id' => 'DESC');
    $CI->standard_model->set_query_data($data_query);
    $result = $CI->standard_model->select();          
    
    return $result;     
}


function you_may_also_like($article_id=0) {
    $CI =& get_instance();
    $CI->load->model('standard_model');
    $data_query['table']  = 'articles';
    $data_query['field']  = 'articles.id,title,articles.cover_image,articles.slug,articles.description,
            date_of_action,categories.name as category, categories.slug as cat_slug, date_of_creation';
    $data_query['join']   = array(
        'categories' => 'articles.cat_id=categories.id'
    );
    $data_query['condition'] = array(                 
        'articles.id !=' => $article_id, 
        'articles.status' => 'active',
    );
    
    $data_query['limit']      = 6;
    $CI->standard_model->set_query_data($data_query);
    $data_result = $CI->standard_model->select();

    if(is_array($data_result)){
        $data_results = $data_result;
    } else {
        $data_results[0] = $data_result;
    }

    return $data_results;       
}
?>