<?php 
function normalize_str($str) {    
    $invalid = array('â€™'=>"'", 'â€˜' => "'", '’' => '\'', '‘'=>'\'', '“' => "\"", '”' => "\"", '—' => '-');        
    $str = str_replace(array_keys($invalid), array_values($invalid), $str);        
    return html_entity_decode($str);    
}

function no_html($html) {
    $html = strip_tags($html);
    $html = preg_replace('/[^A-Za-z0-9\. --,]/', ' ', $html);        
    $html = str_replace('&nbsp', '', $html);        
    return $html;
}

function filter_str($str) {
    $invalid = array('â€™'=>"'", 'â€˜' => "'", '’' => '\'', '‘'=>'\'', '“' => "\"", '”' => "\"", '—' => '-', '<p>' => '','</p>' => '','<br>' => '', '</br>' => '',"&nbsp" => '', "<h1>" => '', "</h1>" => '', '<strong>' => '' , '</strong>' => '', ';' => ' ', '"' => '\'',  '>' => '', '<' => '');
    $str = str_replace(array_keys($invalid), array_values($invalid), $str);
    $str = preg_replace('/\s+/', ' ', trim($str));
    return strip_tags($str);
} 

function truncate_text($text, $length) {
       $length = abs((int)$length);
       if(strlen($text) > $length) {
          $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
       }
       return($text);
    }

function datify($date) {    
    return date('F d, Y', strtotime($date));
}

function slugify($text) {
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) { return 'n-a'; }
    return $text;
}

function getPager($totalPages, $currentPage, $pageSize=5) {
    if ($totalPages <= $pageSize) {
        $startPage = 1;
        $endPage = $totalPages;
    } else {
        if ($currentPage <= 3) {
            $startPage = 1;
            $endPage = $pageSize;
        } else if ($currentPage + 2 >= $totalPages) {
            $startPage = $totalPages - 4;
            $endPage = $totalPages;
        } else {
            $startPage = $currentPage - 2;
            $endPage = $currentPage + 2;
        }
    }
    
    $pages = range($startPage, $endPage); 
    return array(
        'startPage' => $startPage,
        'currentPage' => $currentPage,            
        'endPage' => $endPage,            
        'totalPages' => $totalPages,
        'pages' => $pages
    );
}

function setPager($total_pages=1, $page_no=1, $slug='') {    
    $pager = getPager($total_pages, $page_no);
    $search = '';
    $CI =& get_instance();
    if($CI->input->get('s')) {
        $search = '?s='.$CI->input->get('s');        
    }

    if($slug =='') {
        $slug = '';
    } else {
        $slug = $slug . '/';
    }

    if($pager['totalPages'] > 1 && sizeof($pager['pages'])>0) {
        $viewData = '<div class="jeg_navigation jeg_pagination jeg_pagenav_1 jeg_aligncenter no_navtext no_pageinfo">
                        <span class="page_info">Page '.$page_no.' of '.$total_pages.'</span>';

        if($pager['currentPage'] != 1){
            // if($pager['totalPages']>5){
            //     $viewData .= '<a class="page_number" href="' . base_url() . $slug . $search .  '"><i class="icon ui-icon-angle-double-left"></i> First</a>';
            // }      
            $viewData .= '<a class="page_nav prev" href="' . base_url() .  $slug . ($pager['currentPage'] - 1) . $search .'"> <span class="navtext">Prev</span> </a>';
        }
                            
        foreach ($pager['pages'] as $page) {             
            if($pager['currentPage'] == $page) {
                $viewData .= '<span class="page_number active">' . $page . '</span>';
            } else {
                $viewData .= '<a class="page_number" href="'. ($page==1 ? (base_url() . $slug . $page.$search) : (base_url() . $slug . $page . $search)).'">'. $page .'</a>';
            }
        }

        if($pager['currentPage'] != $pager['totalPages']) {
            $viewData .= '<a class="page_nav next" href="'. base_url() . $slug . ($pager['currentPage'] + 1) . $search .'"> <span class="navtext">Next</span> </i></a>';
            // if($pager['totalPages']>5) {
            //     $viewData .= '<a class="page_number" href="'. base_url(). $slug . $total_pages . $search .'">Last <i class="icon ui-icon-angle-double-right"></i></a>';
            // }
        }
        $viewData .= '</div></div>';

        echo $viewData;
    }
}

function get_page_params($page_no=1) {
    $get_params = '';
    $get_params = get_all_get('page');        
    
    if($page_no!=1){
        if($get_params == ''){
            $get_params .= '?page='.$page_no;
        } else {
            $get_params .= '&page='.$page_no;
        }
    }
    return $get_params;
    // $page_params = $get_params . $page_slug . $page_no;
    // return $page_params;

}

function get_all_get($exlude='') {
    $firstRun = true; 

    $output = ''; 
    
    foreach($_GET as $key=>$val) { 
        if($exlude!=$key) {
            if(!$firstRun) { 
                $output .= "&"; 
            } else { 
                $firstRun = false; 
            } 
            
            $output .= $key."=".$val;            
        }
    } 
    
    if($output=='') {
        return '';
    } else {
        return '?' . $output;
    }
}   
function timify($date) {
    $date = strtotime($date);
    $now = time();
    $diff = $now - $date;

    if ($diff < 60){
        return sprintf($diff > 1 ? '%s seconds ago' : 'a second ago', $diff);
    }

    $diff = floor($diff/60);

    if ($diff < 60){
        return sprintf($diff > 1 ? '%s minutes ago' : 'one minute ago', $diff);
    }

    $diff = floor($diff/60);

    if ($diff < 24){
        return sprintf($diff > 1 ? '%s hours ago' : 'an hour ago', $diff);
    }

    $diff = floor($diff/24);

    if ($diff < 7){
        return sprintf($diff > 1 ? '%s days ago' : 'yesterday', $diff);
    }

    if ($diff < 30)
    {
        $diff = floor($diff / 7);

        return sprintf($diff > 1 ? '%s weeks ago' : 'one week ago', $diff);
    }

    $diff = floor($diff/30);

    if ($diff < 12){
        return sprintf($diff > 1 ? '%s months ago' : 'last month', $diff);
    }

    $diff = date('Y', $now) - date('Y', $date);

    return sprintf($diff > 1 ? '%s years ago' : 'last year', $diff);
}
?>