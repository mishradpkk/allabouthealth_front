<?php 

function page_ads($page_no=1){
    $ads = array();

    // $isMobile = (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
    //                 '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
    //                 '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] );

    // if($isMobile) {
    //     $size = "[[320, 50], [1, 1], [300, 250], [320, 100]]";
    // } else {
    //     $size = "[[728, 90], [970, 90]]";
    // }

    $size = "[[1, 1], [300, 50], [320, 100], [320, 50], [336, 280], [300, 250]]";

    // Anchor Ad
    $ads['anchor']['head'] = "googletag.defineSlot('/21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Video', [[320, 50], [336, 280], [300, 250], [320, 100], [1, 1]], 'div-gpt-ad-1590520399685-0').addService(googletag.pubads());\n";
    $ads['anchor']['body'] = "<!-- /21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Video --> <div id='div-gpt-ad-1590520399685-0'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1590520399685-0'); });</script></div>";

    $ads['1x1']['head'] = "";
    $ads['1x1']['body'] = "";
           
    $ads['sticky']['head'] = "googletag.defineSlot('/21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Sticky', [320, 50], 'div-gpt-ad-1590603062495-0').addService(googletag.pubads());\n";
    $ads['sticky']['body'] = "<!-- /21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Sticky --> <div id='div-gpt-ad-1590603062495-0' style='width: 320px; height: 50px;'> <script> googletag.cmd.push(function() { googletag.display('div-gpt-ad-1590603062495-0'); }); </script></div>";


    $ads['infinite_ad1']['slotCode'] = "/21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Banner";
    $ads['infinite_ad1']['adId'] = 'div-gpt-ad-1590602991870-0';
    $ads['infinite_ad1']['size'] = $size;
    $ads['infinite_ad1']['head'] = "googletag.defineSlot('".$ads['infinite_ad1']['slotCode']."', ".$size.", '".$ads['infinite_ad1']['adId']."').addService(googletag.pubads());\n";
    $ads['infinite_ad1']['body'] = "<!-- ".$ads['infinite_ad1']['slotCode']." --><div id='".$ads['infinite_ad1']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['infinite_ad1']['adId']."'); });</script></div>";


    $ads['banner_ad1']['slotCode'] = "/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad1";
    $ads['banner_ad1']['adId'] = 'div-gpt-ad-1604156922114-0';
    $ads['banner_ad1']['size'] = "[[320, 50], [336, 280], [300, 250], [1, 1], [320, 100]]";
    $ads['banner_ad1']['head'] = "googletag.defineSlot('".$ads['banner_ad1']['slotCode']."', ".$size.", '".$ads['banner_ad1']['adId']."').addService(googletag.pubads());\n";
    $ads['banner_ad1']['body'] = "<!-- ".$ads['banner_ad1']['slotCode']." --><div id='".$ads['banner_ad1']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['banner_ad1']['adId']."'); });</script></div>";

    $ads['banner_ad2']['slotCode'] = "/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad2";
    $ads['banner_ad2']['adId'] = 'div-gpt-ad-1604156961549-0';
    $ads['banner_ad2']['size'] = "[[320, 100], [1, 1], [320, 50], [336, 280], [300, 250]]";
    $ads['banner_ad2']['head'] = "googletag.defineSlot('".$ads['banner_ad2']['slotCode']."', ".$size.", '".$ads['banner_ad2']['adId']."').addService(googletag.pubads());\n";
    $ads['banner_ad2']['body'] = "<!-- ".$ads['banner_ad2']['slotCode']." --><div id='".$ads['banner_ad2']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['banner_ad2']['adId']."'); });</script></div>";
    

    $ads['ad1']['slotCode'] = "/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad1";
    $ads['ad1']['adId'] = 'div-gpt-ad-1590520482080-0';
    $ads['ad1']['size'] = $size;
    $ads['ad1']['head'] = "googletag.defineSlot('".$ads['ad1']['slotCode']."', ".$size.", '".$ads['ad1']['adId']."').addService(googletag.pubads());\n";
    $ads['ad1']['body'] = "<!-- ".$ads['ad1']['slotCode']." --><div id='".$ads['ad1']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad1']['adId']."'); });</script></div>";

    $ads['ad2']['slotCode'] = "/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad2";
    $ads['ad2']['adId'] = 'div-gpt-ad-1590520652112-0';
    $ads['ad2']['size'] = $size;
    $ads['ad2']['head'] = "googletag.defineSlot('".$ads['ad2']['slotCode']."', ".$size.", '".$ads['ad2']['adId']."').addService(googletag.pubads());\n";
    $ads['ad2']['body'] = "<!-- ".$ads['ad2']['slotCode']." --><div id='".$ads['ad2']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad2']['adId']."'); });</script></div>";

    $ads['ad3']['slotCode'] = "/21814398523/THE_VIRAL_STREET/TVS_Ad3";
    $ads['ad3']['adId'] = 'div-gpt-ad-1590520710000-0';
    $ads['ad3']['size'] = $size;
    $ads['ad3']['head'] = "googletag.defineSlot('".$ads['ad3']['slotCode']."', ".$size.", '".$ads['ad3']['adId']."').addService(googletag.pubads());\n";
    $ads['ad3']['body'] = "<!-- ".$ads['ad3']['slotCode']." --><div id='".$ads['ad3']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad3']['adId']."'); });</script></div>";

    $ads['ad4']['slotCode'] = "/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad4";
    $ads['ad4']['adId'] = 'div-gpt-ad-1590520765482-0';
    $ads['ad4']['size'] = $size;
    $ads['ad4']['head'] = "googletag.defineSlot('".$ads['ad4']['slotCode']."', ".$size.", '".$ads['ad4']['adId']."').addService(googletag.pubads());\n";
    $ads['ad4']['body'] = "<!-- ".$ads['ad4']['slotCode']." --><div id='".$ads['ad4']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad4']['adId']."'); });</script></div>";

    $ads['ad5']['slotCode'] = "/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad5";
    $ads['ad5']['adId'] = 'div-gpt-ad-1590602766827-0';
    $ads['ad5']['size'] = $size;
    $ads['ad5']['head'] = "googletag.defineSlot('".$ads['ad5']['slotCode']."', ".$size.", '".$ads['ad5']['adId']."').addService(googletag.pubads());\n";
    $ads['ad5']['body'] = "<!-- ".$ads['ad5']['slotCode']." --><div id='".$ads['ad5']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad5']['adId']."'); });</script></div>";

    /*
    $ads['ad6']['slotCode'] = "/21814398523/IS_Geekfacts_ad6";
    $ads['ad6']['adId'] = 'div-gpt-ad-1589954118994-0';
    $ads['ad6']['size'] = $size;
    $ads['ad6']['head'] = "googletag.defineSlot('".$ads['ad6']['slotCode']."', ".$size.", '".$ads['ad6']['adId']."').addService(googletag.pubads());\n";
    $ads['ad6']['body'] = "<!-- ".$ads['ad6']['slotCode']." --><div id='".$ads['ad6']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad6']['adId']."'); });</script></div>";

    $ads['ad7']['slotCode'] = "/21814398523/IS_Geekfacts_ad7";
    $ads['ad7']['adId'] = 'div-gpt-ad-1571836564690-0';
    $ads['ad7']['size'] = $size;
    $ads['ad7']['head'] = "googletag.defineSlot('".$ads['ad7']['slotCode']."', ".$size.", '".$ads['ad7']['adId']."').addService(googletag.pubads());\n";
    $ads['ad7']['body'] = "<!-- ".$ads['ad7']['slotCode']." --><div id='".$ads['ad7']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad7']['adId']."'); });</script></div>";
    
    $ads['ad8']['slotCode'] = "/21814398523/IS_Geekfacts_ad8";
    $ads['ad8']['adId'] = 'div-gpt-ad-1571836629016-0';
    $ads['ad8']['size'] = $size;
    $ads['ad8']['head'] = "googletag.defineSlot('".$ads['ad8']['slotCode']."', ".$size.", '".$ads['ad8']['adId']."').addService(googletag.pubads());\n";
    $ads['ad8']['body'] = "<!-- ".$ads['ad8']['slotCode']." --><div id='".$ads['ad8']['adId']."'><script>googletag.cmd.push(function() { googletag.display('".$ads['ad8']['adId']."'); });</script></div>";
    */
    
    return $ads;
}

