<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Instant extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('standard_model');
        $this->load->library('memcached_library');
        $this->load->helper('utilities');
    }

    function index(){
        redirect(base_url());
    }

    public function article($article_slug='')
    {        
        $this->load->view('instant_test');      
    }

    public function rssfeed() {
        
        $data['table']  = 'articles';
        $data['field']  = 'articles.id,title,articles.cover_image,slug,language,date_of_creation, date(date_of_creation) as c_date , time(date_of_action) as c_time,articles.tags,
                date_of_action,user.first_name, user.full_name,user.id as user_id,categories.id as cat_id,categories.name as cat_name, articles.description, articles.short_description';
        $data['join']   = array(
                'users'.' as user' => 'articles.user_id=user.id',
                'categories' => 'articles.cat_id=categories.id', 
            );

        $data['condition'] = array(
            'articles.status' => 'active',            
        ); 
        if ($this->input->get('id')) {
            $data['condition']['articles.id'] = $this->input->get('id');        
        } else {
            $data['order_by'] = array('date_of_action' => 'desc' ); 
            
            $data['offset'] = 0;        
            if ($this->input->get('offset')) {
                $data['offset'] = $this->input->get('offset');
            }
            
            $data['limit'] = 50;
            if ($this->input->get('limit')) {
                $data['limit'] = $this->input->get('limit');
            }            
        }

        $this->standard_model->set_query_data($data);
        $data_results = $this->standard_model->select();

        $stories = array();
        if (is_array($data_results)) {
          $stories = $data_results;
        }else{
          $stories[0] = $data_results;
        }      
        if($this->input->get('utm_term')) {
            $utm_name = $this->input->get('utm_term');
            $utm_term = "?utm_source=".$utm_name."&utm_term=".$utm_name."&utm_campaign=".$utm_name."&utm_medium=".$utm_name;
        } else {
            $utm_term = "?utm_source=InstantArticle&utm_term=InstantArticle&utm_campaign=InstantArticle&utm_medium=InstantArticle";
        }
        
        
        $this->load->library('Xml_writer');
        header('Content-type: text/xml; charset=utf-8');
        $xml = new Xml_writer;
        $xml->initiate();
        $data['last_update'] = $stories[0]->date_of_action;

        foreach ($stories as $story) {

            $images = $this->article_media($story->id);
            $related_article = $this->related_article($story->cat_id, $story->id, $story->language);

            $xml->startBranch('item');
            $xml->addNode('title', $story->title);

            $story_link = base_url().''.slugify($story->cat_name).'/'.$story->slug. $utm_term;

            $xml->addNode('link', $story_link);
            $utm_param_name = $this->input->get('utm_term');

            if ($utm_param_name) {
                
                $xml->addNode('coverImages', ' <image title="'.$story->title.'">'.base_url() . 'assets/uploads'.$story->id.'/'.$story->cover_image.'</image>');                    
                $xml->addNode('dc:creator', $story->full_name);
                $xml->addNode('georss:point', '22.7228 75.8869');
                
                $xml->addNode('category', $story->cat_name);
            }else{                
                $xml->addNode('coverimage', base_url() . 'assets/uploads'. $story->id.'/'.$story->cover_image);
            }
            $xml->addNode('guid', $story->id);
            $xml->addNode('pubDate', date("D, d M Y H:i:s", strtotime($story->date_of_creation)).' GMT');
            
            $xml->addNode('author', $story->full_name);

            $data['story_id'] = $story->id;
            $data['story_title'] = $story->title;
            $data['tags'] = $story->tags;
            $data['story_link'] = $story_link;
            $data['date_of_creation'] = $story->c_date;
            $data['date_of_action'] = $story->c_time;
            $data['user_full_name'] = $story->full_name;
            $data['story_desc'] = normalize_str($story->description);

            $data['short_desc'] = normalize_str($story->short_description);
            $data['cover_image'] = $story->cover_image;

            $data['ga'] = '';
            $this->load->helper('settings');             
            $meta = meta(); 


            $data['ga'] = ' (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,"script","https://www.google-analytics.com/analytics.js","ga");  ga("create", "UA-162270014-3", "auto"); ga("send", "pageview");';
           

            $final_cat = explode('&', $story->cat_name);
            if (sizeof($final_cat) == 3) {
                $cat_name = $final_cat[0].'&'.$final_cat[1].'&'.$final_cat[2];
            }else if (sizeof($final_cat) == 2) {
                $cat_name = trim($final_cat[0]).'&'.trim($final_cat[1]);

            }else{
                $cat_name = trim($final_cat[0]);
            }
            $data['cat_name'] = $cat_name;

            
            $xml->addNode('content:encoded',$this->rss_view($data,$images,$related_article,$utm_param_name));
            $xml->endBranch();             
        }
        $data['xml'] = $xml->getXml();
        $data['base_url'] = base_url();

        $xml_data = $this->load->view('instant_rss', $data);
    }    

    private function article_media($article_id){
        $file_name = "INSTANT_media_article_".$article_id;

        $mem_cache_data = $this->memcached_library->get($file_name);
        if (!$mem_cache_data) {
            $data['table']      = 'media';
            $data['field']      = 'id,post_id,media,source,caption,description,doc, media_type, alt_attribute, height, width';
            $data['condition']  =  array( 'post_id' => $article_id, 'status' => '1');
            $data['order_by']   = array('media_order' => 'ASC');      

            $this->standard_model->set_query_data($data);
            $data_result = $this->standard_model->select();

            if(is_array($data_result)) {
                $data_results = $data_result;
            } else {
                $data_results[0] = $data_result;
            }

            $this->memcached_library->add($file_name, $data_results, CACHE_LIMIT);

            return $data_results;
        } else {
            return $mem_cache_data; 
        }
    }

    public function related_article($cat_id=0, $article_id=0, $language='') {
        $file_name = "INSTANT_related_article_".$article_id."_".$cat_id."_". $language;


        $mem_cache_data = $this->memcached_library->get($file_name);
        if (!$mem_cache_data) {
            $data['table']  = 'articles';
            $data['field']  = 'articles.id,title,articles.cover_image,slug,categories.name as cat_name';
            $data['join']   = array(
                    // 'users'.' as user' => 'articles.user_id=user.id',
                    'categories' => 'articles.cat_id=categories.id', 
                );
            $data['condition'] = array(
                'articles.cat_id' => $cat_id,
                'articles.id !=' => $article_id,
            );
            if($language == 'Hindi' ){
                $data['condition']['articles.language'] = 'Hindi';
            }
            $data['limit'] = 3;
            $this->standard_model->set_query_data($data);
            
            $data_result = $this->standard_model->select();
            
            if(is_array($data_result)){
                $data_results = $data_result;
            } else {
                $data_results[0] = $data_result;
            }
            $this->memcached_library->add($file_name, $data_results, CACHE_LIMIT);

            return $data_results;
        } else {
            return $mem_cache_data; 
        }
    }

    public function rss_view($data,$images,$related_article,$utm_term=null){

        if ($utm_term != null || $utm_term != false) {
            
            $html =  '
            <![CDATA[';

            $html .= '<img src="'.base_url() . 'assets/uploads'.$data['story_id'].'/'.$data['cover_image'].'" data-title="'.$data['story_title'].'"/>';

            $html .= html_entity_decode($data['story_desc']);

            foreach ($images as $media) {

                    if ($media->media_type == 'other'){
                         $ext = pathinfo($media->media, PATHINFO_EXTENSION);
                        if($ext == 'gif') {
                          $thumbnail=base_url() . 'assets/uploads'.$data['story_id']."/".$media->media;
                          $story_media=base_url() . 'assets/uploads'.$data['story_id']."/".$media->media;
                        } else {
                            $thumbnail=base_url() . 'assets/uploads'.$data['story_id']."/200/".$media->media;
                            $story_media=base_url() . 'assets/uploads'.$data['story_id']."/".$media->media;
                        }

                        $html .= html_entity_decode($media->caption).'
                              <img src="'.$story_media.'" width="100" height="100"/>
                          '.html_entity_decode($media->description);
                    } else if ($media->media_type == 'youtube') {

                        $html .= html_entity_decode($media->caption).'
                            <res-iframe type="video"
                                      data-src="'.S3_VIDEO_PATH.$media->media.'"
                                      data-title="'.$media->caption.'" 
                                      data-poster="video poster" 
                                      data-runtime="video runtime" 
                                      data-width="100"
                                      data-height="100"  >
                          </res-iframe>
                        '.html_entity_decode($media->description);
                    } else if ($media->media_type == 'twitter'){

                        $html .= html_entity_decode($media->caption).'
                                    <article class="w-article-content">
                                        <blockquote class="twitter-tweet" data-lang="en">
                                            <a href="https://twitter.com/'.$media->media.'" width="2000" height="100"></a>
                                        </blockquote>
                                        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                    </article>
                                     '.html_entity_decode($media->description);

                    } else if ($media->media_type == 'instagram'){

                        $html .= html_entity_decode($media->caption).'
                                    <article class="w-article-content">
                                        <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px); width:calc(100% - 2px);">
                                            <a href="https://www.instagram.com/'.$media->media.'" width="100" height="100" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; target="_blank"></a>
                                        </blockquote>
                                        <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
                                    </article>
                                    '.html_entity_decode($media->description);
                                    
                    }else if ($media->media_type == 'facebook') {
                        $html .= html_entity_decode($media->caption).
                        '<article class="w-article-content">
                              <iframe src="https://www.facebook.com/plugins/post.php?href=https://www.facebook.com/'.$media->media.'&width=500" width="500" height="437"
                        style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </article>'.html_entity_decode($media->description);
                    }
                 }

            
            return $html.']]';

        }else{
            $short_desc_text = '';
            if(isset($data['short_desc']) && $data['short_desc']!='') {
                $short_desc_text = '<h2>'.$data['short_desc'].'</h2>';
            }
            $html =  '
            <!doctype html>
            <html lang="en" prefix="op: //media.facebook.com/op#">
              <head>
                <meta charset="utf-8">
                <!-- URL of the web version of this article -->
                <!-- TODO: Change the domain to match the domain of your website -->
                <link rel="canonical" href="'.$data['story_link'].'">
                <meta property="op:markup_version" content="v1.0">
                <meta property="fb:use_automatic_ad_placement" content="enable=true ad_density=default">
                <meta property="fb:op-recirculation-ads" content="placement_id=1742308636031210_1881465168782222">
                <meta property="fb:likes_and_comments" content="enable">
                <meta property="op:tags" content="'.$data['tags'].'">
                <meta property="fb:article_style" content="holiviral">
                <meta property="og:title" content="'.$data['story_title'].'">
                <meta property="og:description" content="'.filter_str($data['story_desc']).'">
                <meta property="og:image" content='.base_url() . 'assets/uploads'.$data['story_id'].'/'.$data['cover_image'].'>
              </head>
              <body>
                <article>
                  <header>
                        
                    <h1>'.$data['story_title'].'</h1>'.
                    $short_desc_text.
                    '<h3 class="op-kicker">'.$data['cat_name'].'</h3>
                    <address>
                      <a>'.$data['user_full_name'].'</a>
                      <a>'.$data['date_of_creation'].'</a>
                    </address>
                    <time class="op-published" datetime="'.$data['date_of_creation'].'"></time>
                    <time class="op-modified" dateTime="'.$data['date_of_action'].'"></time>
                    <figure data-feedback="fb:likes,fb:comments">
                      <img src='.base_url() . 'assets/uploads'.$data['story_id'].'/'.$data['cover_image'].' />
                    </figure>
                      '.$data['story_desc'].'
                        <figure class="op-ad">
                            <iframe width="300" height="250" style="border:0; margin:0;" src="https://www.facebook.com/adnw_request?placement=1742308636031210_1867008893561183&adtype=banner300x250"></iframe>
                        </figure>
                  </header>';

                  foreach ($images as $media) {

                    if ($media->media_type == 'other'){
                         $ext = pathinfo($media->media, PATHINFO_EXTENSION);
                        if($ext == 'gif') {
                          $thumbnail=base_url() . 'assets/uploads'.$data['story_id']."/".$media->media;
                          $story_media=base_url() . 'assets/uploads'.$data['story_id']."/".$media->media;
                        } else {
                          $thumbnail=base_url() . 'assets/uploads'.$data['story_id']."/200/".$media->media;
                              $story_media=base_url() . 'assets/uploads'.$data['story_id']."/".$media->media;
                        }
                        $figcaption = "";
                        if(isset($media->caption) && $media->caption!=""){
                            $figcaption = '<figcaption class="op-vertical-above op-medium">'.filter_str($media->caption).'</figcaption>';
                        }
                        $html .= '<figure data-feedback="fb:likes,fb:comments">'
                                .$figcaption.
                              '<img src='.$story_media.' width="'.$media->width.'" height="'.$media->height.'"/>
                        </figure>'.normalize_str($media->description);
                    } else if ($media->media_type == 'youtube') {
                        $figcaption = "";
                        if(isset($media->caption) && $media->caption!=""){
                            $figcaption = '<figcaption class="op-medium">'.filter_str($media->caption).'</figcaption>';
                        }
                        $html .= '<figure class="op-interactive">'
                                    .$figcaption.
                                        '<iframe src='.S3_VIDEO_PATH.$media->media.' width="200" height="200"></iframe>
                                </figure>'.normalize_str($media->description);
                                

                    } else if ($media->media_type == 'facebook'){

                         $class_type = "fb-post";
                         if (strpos($media->media, 'videos') === true) {
                             $class_type = "fb-video";
                         }
                        $figcaption = "";
                        if(isset($media->caption) && $media->caption!=""){
                            $figcaption = '<figcaption class="op-vertical-above op-medium">'.filter_str($media->caption).'</figcaption>';
                        }
                        $html .= '<figure class="op-interactive">'
                                    .$figcaption.
                                    '<iframe>
                                        <script src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.5" async></script> 
                                        <div class="'.$class_type.'" data-href="https://www.facebook.com/'.$media->media.'" data-width="500">
                                         </div>
                                    </iframe>
                                     
                                </figure>'.normalize_str($media->description);

                    } else if ($media->media_type == 'twitter') {
                        $figcaption = "";
                        if(isset($media->caption) && $media->caption!=""){
                            $figcaption = '<figcaption class="op-vertical-above op-medium">'.filter_str($media->caption).'</figcaption>';
                        }
                        $html .= '<figure class="op-interactive" >'
                                    .$figcaption.
                                    '<figcaption class="op-vertical-above op-medium">'.filter_str($media->caption).'</figcaption>
                                    <iframe>
                                        <blockquote class="twitter-tweet" lang="en">
                                            <a href="https://twitter.com/'.$media->media.'" width="2000" height="100"></a>
                                        </blockquote>
                                        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                    </iframe>
                                </figure>'.normalize_str($media->description);

                    } else if ($media->media_type == 'instagram'){
                         $figcaption = "";
                        if(isset($media->caption) && $media->caption!=""){
                            $figcaption = '<figcaption class="op-vertical-above op-medium">'.filter_str($media->caption).'</figcaption>';
                        }
                        $html .= '<figure class="op-interactive">'
                                    .$figcaption.
                                    '<iframe>
                                        <blockquote class="instagram-media" data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                                            <a href="https://www.instagram.com/'.$media->media.'" width="100" height="100" target="_blank"></a>
                                        </blockquote>
                                        <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
                                    </iframe>
                                </figure>'.normalize_str($media->description);
                    }
                 }
                 $html .= '                
                <figure class="op-tracker">
                    <iframe>
                         <script>'
                             .$data['ga'].
                       '</script>
                     </iframe>  
                </figure>                
                  <footer>';
                if(sizeof($related_article)>0) {
                $html .= '</ul><ul class="op-related-articles">';
                    $count=1;
                    foreach ($related_article as $article) {
                        $related_article_url = base_url().slugify($article->cat_name).'/'.$article->slug;
                        if ($count == 1) {
                            $html .= '<li data-sponsored="true"><a href="'.$related_article_url.'"></a></li>';
                        }else{
                            $html .= '<li><a href="'.$related_article_url.'"></a></li>';
                        }
                        $count++;
                    }
                $html .= '</ul>';
                }
                 $html .= '</footer>
                    </article>
                </body>
            </html>';
            return $html;
        }
        
    }
}?>
 