<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cloner extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('standard_model');
        $this->load->library('memcached_library');
        $this->load->helper('utilities');
    }

    function index(){
        redirect(base_url());
    }

    public function article() {
        // $url = $this->input->get('url');
        $url = "https://www.viralscroll.com/restaurants-and-bars-that-failed-at-simple-things/150";
        $data['slug'] = "restaurants-and-bars-that-failed-at-simple-things";

        $this->load->helper('simple_html_dom');
        $this->load->helper('utilities');


        $html = file_get_html($url);

        $e = $html->find('.jeg_post_title')[0];
            $data['title'] = $e->innertext;

        $e = $html->find('.jeg_post_subtitle')[0];
            $data['short_description'] = $e->innertext;

        $data['cover_image'] = '';
        if($html->find('meta[property=og:image]')){
            foreach ($html->find('meta[property=og:image]') as $e) {
                $data['cover_image'] = $e->attr['content'];
            }           
        } else {
            if(isset($html->find('.featured_image img')[0])) {
                $e = $html->find('.featured_image img')[0];
                    $data['cover_image'] = $e->src;             
            }           
        }
        $media_ele = $html->find('.alm-nextpage')[0];
        $desc = $media_ele->find('p')[0];
        $data['description'] = '<p>'. $desc->innertext .'</p>';
        
        $media = array();

        $media_caption = '';
        $media_image = '';
        $media_source = '';
        $media_desc = '';

        if(isset($media_ele->find('h3')[0])){
            $media_caption = $media_ele->find('h3')[0];
            $media_caption = $media_caption->innertext;
        }

        if(isset($media_ele->find('img')[0])){
            $media_image = $media_ele->find('figure img')[0];
            $media_image = $media_image->src;
        }

        if(isset($media_ele->find('figcaption a')[0])){
            $media_source = $media_ele->find('figcaption a')[0];
            $media_source = $media_source->href;
        }

        if(isset($media_ele->find('p')[1])){
            $media_desc = $media_ele->find('p')[1];
            $media_desc = '<p>'. $media_desc->innertext  .'</p>';
        }

        $media['media'] = $media_image;
        $media['source'] = $media_source;
        $media['caption'] = $media_caption; 
        $media['description'] = $media_desc;

        $data['media'][0] = $media;

        $media_ele = $html->find('.alm-nextpage');
        $index=0;
        foreach ($media_ele as $ele) {
            if($index>0) {
                $media = array();

                $media_caption = '';
                $media_image = '';
                $media_source = '';
                $media_desc = '';
                
                if(isset($ele->find('h3')[0])){
                    $media_caption = $ele->find('h3')[0];
                    $media_caption = $media_caption->innertext;
                }

                if(isset($ele->find('img')[0])){
                    $media_image = $ele->find('figure img')[0];
                    $media_image = $media_image->src;
                }

                if(isset($ele->find('figcaption a')[0])){
                    $media_source = $ele->find('figcaption a')[0];
                    $media_source = $media_source->href;
                }

                if(isset($ele->find('p')[0])){
                    $media_desc = $ele->find('p')[0];
                    $media_desc = '<p>'. $media_desc->innertext  .'</p>';
                }

                $media['media'] = $media_image;
                $media['source'] = $media_source;
                $media['caption'] = $media_caption; 
                $media['description'] = $media_desc;

                array_push($data['media'], $media);            
            }
            $index++;
        }

        if($html->find('meta[property=article:section]')){
            $ee = $html->find('meta[property=article:section]')[0];
            $data['category'] = $ee->attr['content'];
        }

        if($html->find('meta[property=article:tag]')){
            $ddd = $html->find('meta[property=article:tag]');
            $tags = array();
            foreach ($ddd as $tag_ele) {
                $tag = $tag_ele->attr['content'];
                array_push($tags, $tag);                
            }
            $data['tags'] = $tags;
        }

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // return;

        $this->save_article($data);

        // $this->update_media($data['media']);
    }


    public function greenlemon() {
        // $url = $this->input->get('url');
        $url = "https://greenlemon.me/36-times-people-tried-their-best-but-still-end-up-with-hilarious-results";
        $data['slug'] = "36-times-people-tried-their-best-but-still-end-up-with-hilarious-results";


        $this->load->helper('simple_html_dom');
        $this->load->helper('utilities');


        $html = file_get_html($url);

        $e = $html->find('.mvp-post-title')[0];
            $data['title'] = $e->innertext;

        $e = $html->find('.mvp-post-excerpt p')[0];
            $data['short_description'] = $e->innertext;


        $data['cover_image'] = '';
        if($html->find('meta[property=og:image]')){
            foreach ($html->find('meta[property=og:image]') as $e) {
                $data['cover_image'] = $e->attr['content'];
            }           
        } else {
            // if(isset($html->find('.featured_image img')[0])) {
            //     $e = $html->find('.featured_image img')[0];
            //         $data['cover_image'] = $e->src;             
            // }           
        }
        $media_ele = $html->find('#mvp-content-main')[0];
        $desc = $media_ele->find('p')[0];
        $data['description'] = '<p>'. $desc->innertext .'</p>';

        $desc = $media_ele->find('p')[1];
        $data['description'] .= '<p>'. $desc->innertext .'</p>';
        
        $media = array();

        // $media_caption = '';
        // $media_image = '';
        // $media_source = '';
        // $media_desc = '';

        // if(isset($media_ele->find('h3')[0])){
        //     $media_caption = $media_ele->find('h3')[0];
        //     $media_caption = $media_caption->innertext;
        // }

        // if(isset($media_ele->find('img')[0])){
        //     $media_image = $media_ele->find('figure img')[0];
        //     $media_image = $media_image->src;
        // }

        // if(isset($media_ele->find('figcaption a')[0])){
        //     $media_source = $media_ele->find('figcaption a')[0];
        //     $media_source = $media_source->href;
        // }

        // if(isset($media_ele->find('p')[1])){
        //     $media_desc = $media_ele->find('p')[1];
        //     $media_desc = '<p>'. $media_desc->innertext  .'</p>';
        // }

        // $media['media'] = $media_image;
        // $media['source'] = $media_source;
        // $media['caption'] = $media_caption; 
        // $media['description'] = $media_desc;

        $data['media'] = array();

        $media_ele = $html->find('#mvp-content-main .wp-block-image');
        
        $index=1;
        foreach ($media_ele as $ele) {

            // if($index>0) {
                $media = array();

                $media_caption = '';
            //     $media_image = '';
            //     $media_source = '';
            //     $media_desc = '';
                
                if(isset($html->find('#mvp-content-main h3')[$index])){
                    $media_caption = $html->find('#mvp-content-main h3')[$index];
                    $media_caption = $media_caption->innertext;
                }

                if(isset($ele->find('img')[0])){
                    $media_image = $ele->find('img')[0];
                    $media_image = $media_image->attr['data-cfsrc'];                    
                }

                if(isset($ele->find('figcaption a')[0])){
                    $media_source = $ele->find('figcaption a')[0];
                    $media_source = $media_source->href;
                }

            //     if(isset($ele->find('p')[0])){
            //         $media_desc = $ele->find('p')[0];
            //         $media_desc = '<p>'. $media_desc->innertext  .'</p>';
            //     }

                $media['media'] = $media_image;
                $media['source'] = $media_source;
                $media['caption'] = $media_caption; 
                // $media['description'] = $media_desc;
                $media['description'] = '';

                array_push($data['media'], $media);            
            // }
            $index++;
        }

        
        $ee = $html->find('span.mvp-post-cat')[0];
        $data['category'] = $ee->innertext;
        
        if($html->find('.mvp-post-tags a')){
            $ddd = $html->find('.mvp-post-tags a');
            // $tag = $e->find('a');
            // $tag = $ddd[0]->innertext;
            // echo "<pre>";
            // print_r($tag);
            // echo "<pre>";
            $tags = array();
            foreach ($ddd as $tag_ele) {
                $tag = $tag_ele->innertext;
                array_push($tags, $tag);                
            }
            $data['tags'] = $tags;
        }

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // return;

        $this->save_article($data);

        // $this->update_media($data['media']);
    }

    private function save_article($data){
        $update_data['title'] = $data['title'];

        if($data['category'] == 'History') {
            $update_data['cat_id'] = 1;
        } else if($data['category'] == 'Knowledge') {
            $update_data['cat_id'] = 2;
        } else if($data['category'] == 'Buzzworthy') {
            $update_data['cat_id'] = 3;
        } else if($data['category'] == 'Hilarious') {
            $update_data['cat_id'] = 4;
        } else if($data['category'] == 'Creativity') {
            $update_data['cat_id'] = 5;
        } else {
            $update_data['cat_id'] = 0;
        }

        $update_data['category'] = $data['category'];
        $update_data['description'] = $data['description'];
        
        if($data['short_description'] !='') {
            $update_data['short_description'] = $data['short_description'];
        } else {
            $update_data['short_description'] = truncate_text($data['description'], 100);
        }

        $update_data['total_media'] = sizeof($data['media']);
        $update_data['user_id'] = 1;
        $update_data['views'] = 0;
         
        $tags = '';
        foreach ($data['tags'] as $tag) {
            if($tags =='') {
                $tags = $tag;
            } else {
                $tags = $tags . ','. $tag;
            }
        }

        $update_data['tags'] = $tags;
        $update_data['slug'] = $data['slug'];
        $update_data['date_of_creation'] = date('Y-m-d H:i:s');
        $update_data['date_of_action'] = date('Y-m-d H:i:s');
        $update_data['status'] = 'active';

        $data_query['table'] = 'articles';
        $data_query['field'] = 'id';

        $data_query['condition'] = array(                 
              "slug" => $update_data['slug']
        );

        $this->standard_model->set_query_data($data_query);
        $results = $this->standard_model->select();

        if(!isset($results->id)) {
            $article_id = $this->standard_model->insert_and_id($update_data);            
            if($article_id) {
                $this->save_cover($data['cover_image'], $article_id);
                $this->save_media($data['media'], $article_id);
                $this->save_tags($data['tags'], $article_id);
                echo $article_id .' ::::::::::::: Inserted';                
            }            
        }

    }

    private function save_cover($cover_image, $article_id){
        $update_data['cover_image'] = $this->downloadFile($cover_image, $article_id);

        $data_query['table'] = 'articles';
        $data_query['condition'] = array(                 
            "id" => $article_id
        );
        $this->standard_model->set_query_data($data_query);

        $this->standard_model->update($update_data);
    }

    private function update_media($medias) {
        foreach ($medias as $data) {
            $update_data['caption'] = $data['caption'];

            $data_query['table'] = 'media';
            $data_query['condition'] = array(                 
                "description" => $data['description']
            );
            $this->standard_model->set_query_data($data_query);

            $this->standard_model->update($update_data);
        }
    }

    private function save_media($medias, $article_id) {
        $index=1;
        foreach ($medias as $data) {
            $data['media'] = str_replace('i0.wp.com/viralarch.b-cdn.net', 'i2.wp.com/www.viralscroll.com', $data['media']);
            $data['media'] = str_replace('i1.wp.com/viralarch.b-cdn.net', 'i2.wp.com/www.viralscroll.com', $data['media']);
            $data['media'] = str_replace('i2.wp.com/viralarch.b-cdn.net', 'i2.wp.com/www.viralscroll.com', $data['media']);
            $data['media'] = str_replace('//viralarch.b-cdn.net', 'http://i2.wp.com/www.viralscroll.com', $data['media']);
            

            $update_data['media'] = $this->downloadFile($data['media'], $article_id);
            if($update_data['media'] && $update_data['media'] !='') {
                $file = FCPATH. 'assets/uploads/' . $article_id .'/' . $update_data['media'];

                if(file_exists($file)) {
                    
                    $update_data['post_id'] = $article_id;
                    $update_data['media_type'] = 'other';
                    list($width, $height) = getimagesize($data['media']);
                    $update_data['height'] = isset($height) ? $height : '';
                    $update_data['width'] = isset($width)? $width : '';
                    $update_data['source'] = isset($data['source'])? $data['source']: '';
                    $update_data['caption'] = isset($data['caption'])? $data['caption'] : '';
                    $update_data['description'] = isset($data['description'])? $data['description']:'';
                    $update_data['media_order'] = $index;
                    $update_data['status'] = 1;
                    $update_data['media_type'] = 'other';
                    
                    $update_data['doc'] = date('Y-m-d H:i:s');

                    $data_query['table'] = 'media';
                    $this->standard_model->set_query_data($data_query);
                    $this->standard_model->insert_and_id($update_data);
                    $index++;
                }
            }

        }
    }

    private function save_tags($tags, $article_id) {
        foreach ($tags as $tag) {
            $update_data['article_id'] = $article_id;
            $update_data['tagname'] = $tag;
            $update_data['status'] = 1;
            $update_data['doc'] = date('Y-m-d H:i:s');

            $data_query['table'] = 'article_tags';
            $this->standard_model->set_query_data($data_query);
            $tag_id = $this->standard_model->insert_and_id($update_data);            
        }
    }

    function downloadFile($url, $article_id) {  
        try {          
            $url = str_replace('i0.wp.com/viralarch.b-cdn.net', 'i2.wp.com/www.viralscroll.com', $url);
            $url = str_replace('i1.wp.com/viralarch.b-cdn.net', 'i2.wp.com/www.viralscroll.com', $url);
            $url = str_replace('i2.wp.com/viralarch.b-cdn.net', 'i2.wp.com/www.viralscroll.com', $url);
            $url = str_replace('//viralarch.b-cdn.net', 'http://i2.wp.com/www.viralscroll.com', $url);            

            if ($fp_remote = fopen($url, 'rb')) {
                $toDir =  FCPATH. 'assets/uploads/' . $article_id;
                $url = explode("?",$url);
                $ext = explode(".",$url[0]);
                $ext = $ext[sizeof($ext)-1];
                $withName = $this->random_string(20) . '.' . $ext;

                $local_file = $toDir ."/" . $withName;
             
                if ($fp_local = fopen($local_file, 'wb')) {
                    
                    while ($buffer = fread($fp_remote, 8192)) {
                        fwrite($fp_local, $buffer);
                    }
             
                    fclose($fp_local);               
                } else {
                    fclose($fp_remote);
                    return false;    
                }
                
                fclose($fp_remote);            
                return $withName;
            } else {
                return false;
            }    
        } catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }    
    }

     private function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

}?>
 