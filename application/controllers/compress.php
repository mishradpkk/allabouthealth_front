<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Compress extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('standard_model');
        $this->load->helper('utilities');
    }

    function index(){
        $offset = 0;
        $limit = 100;
        if($this->input->get('limit')) {
            $limit = $this->input->get('limit');
        }

        if($this->input->get('offset')) {
            $offset = $this->input->get('offset');
        }

        // echo $limit;
        // echo $offset;
        $medias  = $this->get_all_media_by_articles($limit,$offset);
        foreach ($medias as $ms) {
            $this->process_image($ms->id, $ms->cover_image);
            foreach ($ms->media as $m) {
                $this->process_image($ms->id, $m->media);
            }
        }
    }

    public function article($article_id = 0) {
        if($article_id!=0) {
            $data_query['table']    = 'articles';
            $data_query['field']    = 'id, cover_image';
            $data_query['condition'] = array(
                'id' => $article_id
            );
            
            $this->standard_model->set_query_data($data_query);
            $article = $this->standard_model->select();

            if(isset($article->id)) {
                $this->process_image($article_id, $article->cover_image);
                $media_query['table']    = 'media';
                $media_query['field']    = 'media';
                $media_query['condition'] = array(
                    'status' => '1',
                    'media_type' => 'other',
                    // 'is_embed !=' => 1,
                    'post_id' => $article_id
                );
                
                $this->standard_model->set_query_data($media_query);
                $medias = $this->standard_model->select();
                if(is_object($medias)) {
                    $medias = array($medias);
                }
                foreach ($medias as $media) {
                    $this->process_image($article_id, $media->media);
                }
            }

        }
    }
    
    protected function imagick_get_image_object($file_path, $no_cache = false) {
        if (empty($this->image_objects[$file_path]) || $no_cache) {
            $this->imagick_destroy_image_object($file_path);
            $image = new Imagick();
            $image->readImage($file_path);
            $this->image_objects[$file_path] = $image;
        }
        return $this->image_objects[$file_path];
    }

    protected function imagick_destroy_image_object($file_path) {
        $image = @$this->image_objects[$file_path];
        return $image && $image->destroy();
    }

    private function process_image($article_id, $file_name) {
        // $link = IMAGE_PATH . $article_id . '/' . $file_name;
        // echo $link;
        if (!extension_loaded('imagick')){
            echo 'imagick not installed';
            return;
        }

        $type = strtolower(substr(strrchr($file_name, '.'), 1));

        $file_path = UPLOAD_PATH . $article_id . '/' . $file_name;
        // $new_path = UPLOAD_PATH . $article_id . '/compress';
        // if (!file_exists($new_path) && !is_dir($new_path)) {
        //     mkdir($new_path, 0777, true);
        // }
        // $new_file_path = $new_path. '/'. $file_name;
        // $new_file_path = $file_path;

        $wp_image = $this->imagick_get_image_object(
            $file_path,
            !empty($options['no_cache'])
        );
        $wp_image->setImageFormat('webp');
        $wp_image->setImageCompressionQuality(90);
        $wp_image->setOption('webp:method', '6');
        $wp_image->writeImage($file_path.'.webp');

        $image = $this->imagick_get_image_object(
            $file_path,
            !empty($options['no_cache'])
        ); 

        if($type == 'png') {
            $this->compress_png($file_path);
            $compressed = true;
        } else if($type == 'jpg' || $type == 'jpeg') {
            $image_original_quality = $image->getImageCompressionQuality();
            if($image_original_quality > 85 && $image_original_quality <90) {
                $image->setImageCompression(Imagick::COMPRESSION_JPEG);
                // $image->gaussianBlurImage(0.8, 10);
                $image->setImageCompressionQuality(80);
            } else if($image_original_quality > 90) {
                $image->setImageCompression(Imagick::COMPRESSION_JPEG);
                // $image->gaussianBlurImage(0.8, 10);
                $image->setImageCompressionQuality(75);
            } 
            $image->writeImage($file_path);
        }

        echo $file_name .' : 1';

        echo "<br/>";
    }


    function compress_png($path_to_png_file, $max_quality = 90) {
        if (!file_exists($path_to_png_file)) {
            throw new Exception("File does not exist: $path_to_png_file");
        }

        // guarantee that quality won't be worse than that.
        $min_quality = 60;

        // '-' makes it use stdout, required to save to $compressed_png_content variable
        // '<' makes it read from the given file path
        // escapeshellarg() makes this safe to use with any path
        $compressed_png_content = shell_exec("pngquant --quality=$min_quality-$max_quality - < ".escapeshellarg($path_to_png_file));

        if (!$compressed_png_content) {
            throw new Exception("Conversion to compressed PNG failed. Is pngquant 1.8+ installed on the server?");
        }
        file_put_contents($path_to_png_file, $compressed_png_content);
        return $compressed_png_content;
    }

    public function get_all_media_by_articles($limit, $offset) {
        $data_query['table']    = 'articles';
        $data_query['field']    = 'id, cover_image';
        $data_query['limit']    = $limit;
        $data_query['offset']    = $offset;
        $data_query['condition'] = array(
            'status' => 'active',
        );              
        $data_query['order_by'] = array('date_of_action' => 'desc' );   
        
        $this->standard_model->set_query_data($data_query);
        $articles = $this->standard_model->select();
        if(is_object($articles)) {
            $articles = array($articles);
        }

        foreach ($articles as $article) {
            $media_query['table']    = 'media';
            $media_query['field']    = 'media';
            $media_query['condition'] = array(
                'status' => '1',
                'media_type' => 'other',
                // 'is_embed !=' => 1,
                'post_id' => $article->id
            );
            
            $this->standard_model->set_query_data($media_query);
            $medias = $this->standard_model->select();
            if(is_object($medias)) {
                $medias = array($medias);
            }
            $article->media = $medias;
        }
        return $articles;
    }    

}?>
 