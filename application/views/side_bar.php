<div id="jeg_off_canvas" class="normal">
    <a href="#" class="jeg_menu_close"><i class="jegicon-cross"></i></a>
    <div class="jeg_bg_overlay"></div>
    <div class="jeg_mobile_wrapper">
        <div class="nav_wrap">
            <div class="item_main">
                <!-- Search Form -->
                <div class="jeg_aside_item jeg_search_wrapper jeg_search_no_expand round">
                    <a href="#" class="jeg_search_toggle"><i class="fa fa-search"></i></a>
                    <form action="<?php echo base_url(); ?>" method="get" class="jeg_search_form" target="_top">
                        <input name="s" class="jeg_search_input" placeholder="Search..." type="text" value="" autocomplete="off" />
                        <button type="submit" class="jeg_search_button btn"><i class="fa fa-search"></i></button>
                    </form>
                    <!-- jeg_search_hide with_result no_result -->
                    <div class="jeg_search_result jeg_search_hide with_result">
                        <div class="search-result-wrapper"></div>
                        <div class="search-link search-noresult">
                            No Result
                        </div>
                        <div class="search-link search-all-button"><i class="fa fa-search"></i> View All Result</div>
                    </div>
                </div>
                <div class="jeg_aside_item">
                    <ul class="jeg_mobile_menu">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-49 current_page_item menu-item-56">
                            <a href="<?php echo base_url(); ?>" aria-current="page">Home</a>
                        </li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-201"><a href="<?php echo base_url(); ?>category/history">History</a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-202"><a href="<?php echo base_url(); ?>category/knowledge">Knowledge</a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-198"><a href="<?php echo base_url(); ?>category/buzzworthy">Buzzworthy</a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-200"><a href="<?php echo base_url(); ?>category/hilarious">Hilarious</a></li>           
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-68"><a href="#">More</a>
                            <ul class="sub-menu" style="display: none;">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69"><a href="<?php echo base_url(); ?>about">About</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70"><a href="<?php echo base_url(); ?>contact">Contact Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71"><a href="<?php echo base_url(); ?>advertise">Advertise With Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72"><a href="<?php echo base_url(); ?>privacy">Privacy Policy</a></li>                                
                            </ul>
                        </li>                     
                    </ul>
                </div>
            </div>
            <div class="item_bottom">
                <!-- <div class="jeg_aside_item socials_widget square">
                    <a href="https://www.facebook.com/TheViralStreet" target="_blank" class="jeg_facebook"><i class="fa fa-facebook"></i> </a>
                  TheViralStreet.  <a href="https://twitter.com/TheViralStreet" target="_blank" class="jeg_twitter"><i class="fa fa-twitter"></i> </a>
                </div> -->
                <div class="jeg_aside_item jeg_aside_copyright">
                    <p>© 2020 <a href="<?php echo base_url();?>" title="TheViralStreet.com">TheViralStreet.com</a> - All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>