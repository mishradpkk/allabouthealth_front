<rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:georss="http://www.georss.org/georss">
  <channel>    
    <title><?php $this->load->helper('settings'); $meta = meta(); echo $meta->project_name;?></title>
    <link><?php echo base_url();?></link>
    <description>
      One Stop destination to all the viral content.
    </description>
    <language>en-us</language>
    <lastBuildDate><?php echo date("D, d M Y H:i:s", strtotime($last_update));?><?php echo ' '.'GMT';?></lastBuildDate>
    <?php echo $xml; ?> 
  </channel>
</rss>
