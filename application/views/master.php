<?php 
$this->load->helper('utilities');

if ($main_content == 'article_view') {
	$this->load->library('user_agent');	
	$this->load->view('includes/header_article.php');
} else {
	$this->load->view('includes/header_website.php');
}

$this->load->view($main_content);

if ($main_content == 'article_view') {
	$this->load->view('includes/footer_article.php'); 
}else {
	$this->load->view('includes/footer_website.php');
}
?>