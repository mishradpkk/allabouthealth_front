<?php 


  $this->load->helper('ads'); 
  $ads = page_ads($page_param);
  $this->load->library('user_agent');
?>
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
window.googletag = window.googletag || {cmd: []};          
</script> 
<script>
  var slots = {};
    googletag.cmd.push(function() {
<?php

    if($layout == 'single') {    
        echo "      ". $ads['infinite_ad1']['head'];
        if($this->agent->is_mobile()) {?>
          
          
        <?php }

        if($this->input->get('camp') && $this->input->get('camp') == 'tvs') {
          echo "      ". $ads['banner_ad1']['head'];
          echo "      ". $ads['banner_ad2']['head'];
        }


    } else {      
        echo "      ". $ads['anchor']['head'];      
    }
?>
    // googletag.pubads().enableSingleRequest();
    // googletag.companionAds().setRefreshUnfilledSlots(true);
      googletag.pubads().collapseEmptyDivs();
      googletag.enableServices();
      googletag.pubads().setTargeting('article_id', '<?php echo $article->id; ?>');
  <?php if($layout == 'single') { $pagetype = 'single'; } else { $pagetype = 'paginated'; } ?>
    googletag.pubads().setTargeting('pagetype', '<?php echo $pagetype; ?>');
  <?php if($layout != 'single') { ?>
    googletag.pubads().setTargeting('page_no', '<?php echo $page_param; ?>');
  <?php } ?>
  <?php if($this->input->get('utm_campaign')) { ?>
    googletag.pubads().setTargeting('utm_campaign', '<?php echo $this->input->get('utm_campaign');?>');
  <?php } ?>
  <?php if($this->input->get('utm_medium')) { ?>
    googletag.pubads().setTargeting('utm_medium', '<?php echo $this->input->get('utm_medium');?>');
  <?php } ?>
  <?php if($this->input->get('utm_source')) { ?>
    googletag.pubads().setTargeting('utm_source', '<?php echo $this->input->get('utm_source');?>');
  <?php } ?>
  <?php if($this->input->get('utm_hash')) { ?>
    googletag.pubads().setTargeting('utm_hash', '<?php echo $this->input->get('utm_hash');?>');
  <?php } ?>
  <?php if($this->input->get('utm_term')) { ?>
    googletag.pubads().setTargeting('utm_term', '<?php echo $this->input->get('utm_term');?>');
  <?php } ?>
  <?php if($this->input->get('Demo')) { ?>
    googletag.pubads().setTargeting('Demo', '<?php echo $this->input->get('Demo');?>');
  <?php } ?>
     });

    refreshAds = function(refresh_slots) {
        if(typeof(refresh_slots) == "undefined") {
            googletag.pubads().refresh();
        } else {
            googletag.pubads().refresh(refresh_slots);
        }
    };

  <?php //if($layout == 'single' && $this->agent->is_mobile()) { ?>
    
  <?php //} ?>
</script>



<!-- Header Wrapper Tag -->
<script src="https://cdn.adapex.io/hb/aaw.tvs.js" async></script>

