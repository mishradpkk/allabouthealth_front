            <footer>
                <div class="footer-container">
                    
                    <div class="social-nav">
                        <div class="footer-brand-block">
                            <a href="/" style="background: url(<?php echo base_url();?>assets/images/logo.png) no-repeat;background-size: cover;"></a>
                        </div>
                        <span class="social-block">
                            <a class="facebook" target="_blank" rel="noopener noreferrer" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="twitter" target="_blank" rel="noopener noreferrer" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="instagram" target="_blank" rel="noopener noreferrer" href="#"><i class="fab fa-instagram-square"></i></a>
                        </span>
                    </div>
                    
                    <div class="footer-body">
                        <div class="left-section">
                            <div class="newsletter-widget">
                                <h2>Get our wellness newsletter</h2>
                                <p>Filter out the noise and nurture your inbox with health and wellness advice that’s inclusive and rooted in medical expertise.</p>
                                <div class="form-block">
                                    <form action="<?php echo base_url();?>/api/newsletter-signup" method="post" novalidate="">
                                        <div class="input-block">
                                            <input placeholder="Enter your email" aria-label="Enter your email" type="email" />
                                        </div>
                                        <button type="submit">
                                            <span><span>SIGN UP</span></span>
                                        </button>
                                    </form>
                                    <p>Your <a target="_blank" rel="noopener noreferrer" href="<?php echo base_url();?>privacy-policy">privacy</a> is important to us</p>
                                </div>
                            </div>
                        </div>
                        <div class="right-section">
                            <div class="section-container">
                                <div class="container-block">
                                    <ul class="part-left">
                                        <li><a href="<?php echo base_url();?>about">About Us</a></li>
                                        <li><a href="<?php echo base_url();?>health/newsletter-signup">Newsletters</a></li>
                                        <!-- <li><a href="<?php echo base_url();?>directory/topics">Health Topics</a></li> -->
                                        <li><a href="<?php echo base_url();?>about#contact-us">Contact Us</a></li>
                                        <li><a href="<?php echo base_url();?>advertising-policy">Advertising Policy</a></li>
                                    </ul>
                                    <ul class="part-right">
                                        <li><a href="<?php echo base_url();?>privacy-policy">Privacy Policy</a></li>
                                        <!-- <li><a href="/privacy-settings">Privacy Settings</a></li> -->
                                        <li class="footer-desc">
                                            <div>
                                                © 2021 AllAboutHealth. All rights reserved. Our website services, content, and products are for informational purposes only. AllAboutHealth does not provide medical advice,
                                                diagnosis, or treatment. <a target="_blank" rel="noopener noreferrer" href="<?php echo base_url();?>additional-information">See additional information</a>.
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="m-footer-desc">
                                        © 2021 AllAboutHealth. All rights reserved. Our website services, content, and products are for informational purposes only. AllAboutHealth does not provide medical advice,
                                        diagnosis, or treatment. <a target="_blank" rel="noopener noreferrer" href="<?php echo base_url();?>additional-information">See additional information</a>.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="bottom-container">
                        <a class="company-block" target="_blank" rel="nofollow" href="<?php echo base_url();?>">
                            <!-- <img alt="AllAboutHealth" src="<?php echo base_url();?>assets/images/logo.png" width="132" height="50" /> -->
                        </a>
                        <div class="links-block">
                            <div class="top-row">
                                <a target="_blank" rel="nofollow" href="<?php echo base_url();?>about">About</a>
                                <a target="_blank" rel="nofollow" href="<?php echo base_url();?>careers">Careers</a>
                                <a target="_blank" rel="nofollow" href="<?php echo base_url();?>advertise-with-us">Advertise with us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <div id="modal-host" style="display: none;">
        <div class="css-1m2nvfk no-hash">
            <button type="button" class="css-kqf6bc backdrop" tabindex="-1"></button>
            <div class="window-wrapper css-1ub0e8h">
                <div class="css-yya393 window">
                    <button type="button" class="css-1eiym9q icon-hl-close window-close-button"></button>
                    <div class="css-6zk0aq">
                        <form class="css-10klw3m">
                            <div class="css-c2lbbj">
                                <div class="css-dr7j08"></div>
                                <div class="css-1sj1gtd">
                                    <img
                                        alt="AllaboutHealth"
                                        height="18"
                                        src="<?php echo base_url(); ?>assets/images/logo.png"
                                    />
                                    <h3 class="css-t4trxv h1">Get our Wellness Wire newsletter</h3>
                                    <p class="css-cpruvj">Nurture yourself with health tips, wellness advice, and more.</p>
                                    <p class="css-1ngltt4">Nurture yourself with health tips, wellness advice, and more.</p>
                                    <div class="css-1q8t4hg">
                                        <label class="css-jomlpz"><input name="email" required="" type="email" placeholder="Enter your email" class="css-dqw99v" value="" /></label>
                                        <button class="css-xy0npl">
                                            <span class="css-14ktbsh"><span class="css-1huyk6v">SUBSCRIBE</span></span>
                                        </button>
                                    </div>
                                    <div class="css-10izwvm">
                                        <!-- <p class="css-egkp07">Other newsletters you won’t want to miss:</p>
                                        <label class="css-1kmfrjl"><input type="checkbox" name="secondary0" class="css-1law5zt" /><span>Sign up for Men's Health</span></label>
                                        <label class="css-1kmfrjl"><input type="checkbox" name="secondary1" class="css-1law5zt" /><span>Sign up for Women's Wellness</span></label> -->
                                        <div class="css-9jgjtt">
                                            <p class="css-1klwf67">Your <a class="css-1pule1j" target="_blank" rel="noopener noreferrer" href="<?php echo base_url();?>privacy-policy">privacy</a> is important to us</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url();?>assets/js/vendor/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/script.js"></script>
</body>

</html>