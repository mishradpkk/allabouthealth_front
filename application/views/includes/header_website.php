<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url();?>assets/favicons/favicon.ico">

    <title>AllAboutHealth: Medical information and health advice you can trust.</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    
    <link href="<?php echo base_url();?>assets/fontawesome/css/all.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/styles.css?v=1.0" rel="stylesheet">

</head>

<body>
    <div style="display: flex;">
        <div style="flex-grow: 1;">
            <div class="header-container">
                <header id="site-header" class="site-header">
                    <div class="header-bg"></div>
                    <div class="header-row">
                        <div class="header-center">
                            <div class="brand-block">
                                <a class="brand" data-event="|Global Header|Logo Click" href="<?php echo base_url();?>" title="allabouthealth">
                                    <img src="<?php echo base_url();?>assets/images/logo.png">
                                </a>
                            </div>
                        </div>
                        <button data-auto="header-menu" class="header-menu">
                            <svg id="icon-hamburger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 25" width="40" height="25"><path d="M38 3H2L0 2V1l2-1h36l1 1v1l-1 1zm0 11H2l-2-1v-1l2-1h36l1 1v1l-1 1zm0 11H2l-2-1v-1l2-1h36l1 1v1l-1 1z"></path></svg>
                            <svg id="icon-close" style="display: none;" xmlns="http://www.w3.org/2000/svg" width="22" height="40" viewBox="0, 0, 40, 40"><path d="M23.65 20L39 4.66A2.587 2.587 0 1 0 35.34 1L20 16.35 4.66 1A2.587 2.587 0 1 0 1 4.66L16.35 20 1 35.34A2.587 2.587 0 1 0 4.66 39L20 23.65 35.34 39A2.587 2.587 0 1 0 39 35.34z"></path></svg>
                        </button>
                        <div class="subscribe-block">
                            <button class="subscribe-button"><span class="subscribe">Subscribe</span></button>
                        </div>
                        <button class="search-icon header-menu">
                            <svg id="icon-search" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 41 41" width="40" height="40"><path id="a" d="M16.4 28a11.7 11.7 0 1 1 0-23.4 11.7 11.7 0 0 1 0 23.5zm22.9 8l-9.8-10a16 16 0 0 0 3.2-9.6A16.3 16.3 0 0 0 1.7 9C-1.7 15.8.1 24 6 29a16.4 16.4 0 0 0 20.4.4l9.7 10a2.3 2.3 0 0 0 4-1 2.3 2.3 0 0 0-.8-2.2z"></path></svg>
                        </button>
                        <form action="/search" class="search-form">
                            <div class="input-block">
                                <div class="autocomplete-wrapper" style="display: inline-block;">
                                    <input class="autocomplete" name="q1" placeholder="Search "
                                        role="combobox" aria-autocomplete="list" aria-expanded="false"
                                        autocomplete="off" value="" />
                                </div>
                            </div>
                            <div class="button-block">
                                <svg id="icon-search" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 41 41" width="40" height="40"><path id="a" d="M16.4 28a11.7 11.7 0 1 1 0-23.4 11.7 11.7 0 0 1 0 23.5zm22.9 8l-9.8-10a16 16 0 0 0 3.2-9.6A16.3 16.3 0 0 0 1.7 9C-1.7 15.8.1 24 6 29a16.4 16.4 0 0 0 20.4.4l9.7 10a2.3 2.3 0 0 0 4-1 2.3 2.3 0 0 0-.8-2.2z"></path></svg>
                            </div>
                            <button type="submit">
                                <span><span>GO</span></span>
                            </button>
                        </form>
                    </div>
                </header>
            </div>

            <div class="menubar-container">
                <div class="menubar hidden">
                    <div class="menubar-content">
                        <div class="menubar-left">
                            <form action="/search" class="css-1fefdh9">
                                <div class="css-r0bbdu">
                                    <div class="autocomplete-wrapper" style="display: inline-block;">
                                        <input class="autocomplete" name="q1" placeholder="Search Healthline" aria-label="Search Healthline" role="combobox" aria-autocomplete="list" aria-expanded="false" autocomplete="off" value="" />
                                    </div>
                                </div>
                                <button class="css-1cr7p8u" type="submit" aria-label="Click to Search">
                                    <span class="css-14ktbsh"><span class="css-1huyk6v">GO</span></span>
                                </button>
                            </form>

                            <section>
                                <h3>Topics</h3>
                                <ul>
                                    <li><a href="<?php echo base_url();?>yoga">Yoga </a></li>
                                    <li><a href="<?php echo base_url();?>weight-loss">Weight Loss </a></li>
                                    <li><a href="<?php echo base_url();?>exercise">Exercise </a></li>
                                    <li><a href="<?php echo base_url();?>nutrition">Nutrition </a></li>
                                </ul>
                            </section>
                        </div>
                        
                    </div>
                </div>
            </div>

