<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';


// $route['article/(:num)/(:num)/(:any)'] = "story/story_desc/$1/$2";
// $route['article/(:num)/(:num)'] = "story/story_desc/$1/$2";
$route['(:num)'] = "home/search/$1";

$route['home/(:any)'] = "home/$1";
$route['home/(:any)/(:any)'] = "home/$1/$2";

$route['recent-articles/(:any)'] = "home/recent_articles/$1";
$route['recent-articles/(:any)/(:any)'] = "home/recent_articles/$1/$2";

$route['api/(:any)'] = "api/$1";
$route['api/(:any)/(:any)'] = "api/$1/$2";

$route['search/(:any)'] = "home/search/$1";
$route['search/(:any)/(:any)'] = "home/search/$1/$2";

$route['test/api/(:any)'] = "test/api/$1";
$route['test/api/(:any)/(:any)'] = "test/api/$1/$2";

$route['test/(:any)'] = "test/article_data/$1";
$route['test/(:any)/(:any)'] = "test/article_data/$1/$2";

$route['instant'] = "instant";
$route['instant/(:any)'] = "instant/$1";
$route['instant/(:any)/(:any)'] = "instant/$1/$2";

$route['compress'] = "compress";
$route['compress/(:any)'] = "compress/$1";
$route['compress/(:any)/(:any)'] = "compress/$1/$2";

$route['about'] = "home/about_us";
$route['advertise'] = "home/advertise";
$route['contact'] = "home/contact_us";
$route['faq'] = "home/faq";
$route['terms-of-service'] = "home/terms_service";
$route['privacy'] = "home/privacy_policy";
$route['cookie-policy'] = "home/cookie_policy";
$route['bad-ads'] = "home/bad_ads";
$route['careers'] = "home/careers";

$route['sitemap'] = 'meta/sitemap/main_sitemap';
$route['sitemap.xml'] = 'meta/sitemap/main_sitemap';
$route['sitemap.xsl'] = 'meta/sitemap/sitemap_style';
$route['sitemap/category/(:any).xml'] = 'meta/sitemap/category_sitemap/$1';
$route['sitemap/(:any).xml'] = 'meta/sitemap/main_sitemap/$1/$2';
$route['sitemap/(:any).xml'] = 'meta/sitemap/main_sitemap/$1/$2';
$route['page-sitemap.xml'] = 'meta/sitemap/page_sitemap';
$route['category-sitemap.xml'] = 'meta/sitemap/category_sitemap';

$route['category/(:any)'] = "home/category/$1";
$route['category/(:any)/(:any)'] = "home/category/$1/$2";

$route['tag/(:any)'] = "home/tag/$1";
$route['tag/(:any)/(:any)'] = "home/tag/$1/$2";

$route['cloner/(:any)'] = "cloner/$1";
$route['cloner/(:any)/(:any)'] = "cloner/$1/$2";

$route['(:any)'] = "article/route_to/$1";
$route['(:any)/(:any)'] = "article/route_to/$1/$2";

/* End of file routes.php */
/* Location: ./application/config/routes.php */