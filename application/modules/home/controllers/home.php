<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();		 
		$this->load->model('standard_model');
		 
		$this->load->helper('settings');
	}

	public function index($page=0) {
		// $data['meta'] = meta();

		// $data['meta_title'] = $data['meta']->meta_title;
		// $data['meta_description'] = $data['meta']->meta_description;
		// $data['meta_keywords'] = $data['meta']->meta_keywords;
		// $page_no = 1;
		
		// if($this->input->get('s')){
  //       	$data['search_text'] = $this->input->get('s');

		// 	$data['meta_title'] = "Search - ".$data['search_text'];
			
		// 	$search = $this->stories("search", $page_no, 0, 9);
		// 	$data['page_no'] = $page_no;			

		// 	$data['slug'] = 'search';
		// 	$data['section_name'] = 'Search: '.$data['search_text'];
			
		// 	$data['articles']= $search['articles'];	
		// 	$data['total_pages'] = $search['total_pages'];
			
		// 	$data['main_content'] = "search_view";
	 //        $data['footer_articles'] = $this->footer_articles();

  //       } else {
	        $latest	= $this->articles("latest", 4);
	        $data['latest_articles'] = $latest['articles'];
	        // $promoted = $this->stories('promoted', $page_no, 0, 4);
	 //        // $popular = $this->stories("popular", $page_no, 0, 5);
	        $trending = $this->articles("trending", 8);
	 //        // $editors_pick = $this->stories("editors_pick", $page_no, 0, 6);
	 //        $data['promoted_articles'] = $promoted['articles'];
	        $data['trending_articles'] = $trending['articles'];

	        $more_top = $this->articles("latest", 8, 4);
	        $data['more_top'] = $more_top['articles'];
	 //        // $data['editors_pick_articles'] = $editors_pick['articles'];
	 //        $data['latest_articles'] = $latest['articles'];
	        $data['main_content'] = "home_view";
	 //        $data['footer_articles'] = $this->footer_articles();
  //       }

	        // echo "<pre>";
	        // print_r($data);
	        // echo "</pre>";
	        // return;
        $this->load->view('master',$data);
	}


	// public function search($page_no=1) {
	// 	$data['meta'] = meta();
	// 	$data['meta_title'] = $data['meta']->meta_title;
	// 	$data['meta_description'] = $data['meta']->meta_description;
	// 	$data['meta_keywords'] = $data['meta']->meta_keywords;
	// 	if($this->input->get('s')){
 //        	$data['search_text'] = $this->input->get('s');

	// 		$data['meta_title'] = "Search - ".$data['search_text'];
			
	// 		$search = $this->stories("search", $page_no, 0, 9);
	// 		$data['page_no'] = $page_no;			

	// 		$data['slug'] = 'search';
	// 		$data['section_name'] = 'Search: '.$data['search_text'];
			
	// 		$data['articles']= $search['articles'];	
	// 		$data['total_pages'] = $search['total_pages'];
			
	// 		$data['main_content'] = "search_view";
	//         $data['footer_articles'] = $this->footer_articles();

 //        } else {
	//     	redirect('/');
 //        }
 //        $this->load->view('master',$data);
	// }

	// public function recent_articles($param_1 = 0, $param_2=0) {
	// 	$cat_id = 0;
	// 	if($param_2 == 0) {
	// 		$page_num = $param_1;
	// 	} else {
	// 		$cat_id = $param_1;
	// 		$page_num = $param_2;
	// 	}
	// 	$data['load_article'] = $this->stories("latest", $page_num, $cat_id, 12);

	// 	$this->load->view('load_more_list', $data);
	// }

	// public function featured_articles(){
	// 	$cat_id = 0;
	// 	if($this->input->post('cat_id')) {
	// 		$cat_id = $this->input->post('cat_id');
	// 	}
	// 	$section = $this->input->post('section');
	// 	$page_no = $this->input->post('page');
	// 	if($section && $page_no) {
	// 		$limit = 16;
	// 		if($section == 'popular'){
	// 			$limit = 20;
	// 		}
	// 		$load_article = $this->stories($section, $page_no, $cat_id, $limit);
	// 	} else {
	// 		return;
	// 	}
	// 	$this->load->view('load_more_section_list', $load_article);		
	// }

	// private function footer_articles() {
	// 	$data_query['table'] 	= 'articles';
	// 	$data_query['field']	= 'id,title,cover_image,slug';
	// 	$data_query['condition'] = array(
	// 	 	'articles.status' => 'active',		 	
	// 	); 
	// 	$data_query['order_by'] = array('date_of_action' => 'desc' );	
	// 	$data_query['limit'] = 5;
 //    	$data_query['offset'] = 0;	

 //    	$this->standard_model->set_query_data($data_query);
	// 	$data_results = $this->standard_model->select();

	// 	if(is_object($data_results)) {
	// 		$data_results = array($data_results);
	// 	}
	// 	return $data_results;
	// }

	private function articles($type="", $limit=10, $offset=0) {
		$data_query['table'] = 'articles';
		$data_query['field'] = 'articles.id, title, articles.cover_image, articles.slug, short_description, categories.name as category, categories.slug as cat_slug';
		$data_query['join']  = array(
			"categories" => "categories.id = articles.cat_id",			
		);

		if($type == 'latest') {
			$data_query['condition'] = array(
			 	'articles.status' => 'active',
			 	'articles.is_deleted' => 0,
			); 
			$data_query['order_by'] = array('date_of_action' => 'desc' );	
		} else if($type == 'trending') {
			$data_query['condition'] = array(
				'articles.status' => 'active',
				'articles.is_deleted' => 0,
				// 'articles.is_trending' => '1',
			);				
			$data_query['order_by'] = array('date_of_action' => 'desc' );
		}
		// else if($type == 'promoted') {
		// 	$data_query['condition'] 	= array(
		// 		'articles.status' => 'active',
		// 	 	'articles.is_promoted' => '1',
		// 	);
		// 	$data_query['order_by'] = array('date_of_action' => 'desc' );
		// } else if($type == 'trending') {
		// 	$data_query['condition'] = array(
		// 		'articles.status' => 'active',
		// 		'articles.is_trending' => '1',
		// 	);				
		// 	$data_query['order_by'] = array('date_of_action' => 'desc' );
		// } else if($type == 'editors_pick') {
		// 	$data_query['condition'] = array(
		// 		'articles.status' => 'active',
		// 		'articles.is_editors_pick' => '1',
		// 	);				
		// 	$data_query['order_by'] = array('date_of_action' => 'desc' );
		// } else if($type == 'popular') {
		// 	$data_query['condition'] = array(
		// 		'articles.status' => 'active',
		// 	);
		// 	$data_query['order_by'] = array('views' => 'desc' );
		// } else {
		// 	$data_query['condition'] = array(
		// 	 	'articles.status' => 'active',
		// 	); 
		// 	$data_query['order_by'] = array('date_of_action' => 'desc' );
		// }

		// if($this->input->get('s') || strpos($type,'search') !== false){

		// 	if($this->input->get('s')){
		// 		$q_word = $this->input->get('s');
		// 	} else {
		// 		$q_word = str_replace("search: ","", $type);
		// 	}

		// 	$q_arr = explode(" " ,$this->input->get('s'));

		// 	if(sizeof($q_arr) == 1) {
		// 		// $data_query['or_where'] = array(
		// 		//  	'articles.title like' => '% '.$q_word.' %',
	 //   //          );

		// 		$data_query['like'] = array(
		// 		 	'articles.title' => $q_word,
		// 			// 'articles.title ' => $q_word,
	 //            );
	 //   // 			 $data_query['or_like'] = array(
		// 		// 	'articles.title' => ' '.$q_word,
		// 		// );
	            
	            
		// 	} else {
		// 		$data_query['like'] = array(
		// 			'articles.title' => $q_word
	 //            );
		// 	}
		// }

		//  if($cat_id!=0) {	
		// 	$data_query['condition']['articles.cat_id'] = $cat_id;
		// }

		$data_query['limit'] = $limit;
    	$data_query['offset'] = $offset;	

    	$this->standard_model->set_query_data($data_query);
		$data_results = $this->standard_model->select();

		if(is_object($data_results)) {
			$data_results = array($data_results);
		}

		if(sizeof($data_results)<1) {
			$data_results = array();	
		}
		$results['articles'] = $data_results;
		// $this->load->helper('query');
		// $results['total_pages'] = get_total_pages($data_query, $limit);

		return $results;		
	}

	public function category($slug='', $page_no=1) {
		$category = $this->get_category_details($slug);
		$data['meta'] = meta();

		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_description'] = $data['meta']->meta_description;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		
		$data['page_no'] = $page_no;

		// $total_pages

		if(isset($category->id)) {
			$category->slug = $slug;
			$data['category'] = $category;

			$data['meta_title'] = $category->name . " – TheViralStreet";

			$latest	= $this->stories("latest", $page_no, $category->id, 11);
			
			$data['banner_articles'] = array();
			if(sizeof($latest['articles'])>2) {				
				$data['banner_articles'] = array_slice($latest['articles'], 0, 2, true);
		        $data['latest_articles'] = array_splice($latest['articles'], 2);
			} else {
				$data['latest_articles'] = $latest['articles'];
			}

			$data['total_pages'] = $latest['total_pages'];				
			$data['main_content']   = "category_view";		

			$data['footer_articles'] = $this->footer_articles();

			$this->load->view('master',$data);			

		} else {
			// redirect(base_url());
		}
	}

	public function tag($slug='',$page_no=1) {
		$data['meta'] = meta();

		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_description'] = $data['meta']->meta_description;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		
		$data['page_no'] = $page_no;

		$tag_name = str_replace('-', ' ', $slug);
		$data['tag_name'] = ucwords($tag_name);
		
		$data['meta_title'] = 'Tag - '. $data['tag_name'] . " – TheViralStreet";		
		
		$data['main_content']   = "tag_view";		

		$data_query['table'] 	= 'articles';
		$data_query['field']	= 'articles.id,title,articles.cover_image,articles.slug, date_of_action, date_of_creation, articles.user, short_description, categories.name as category, categories.cat_slug';
		
		$data_query['join']  = array(
			"categories" => "categories.id = articles.cat_id",
			"article_tags" => "article_tags.article_id = articles.id"
		);

		$data_query['condition'] = array(
		 	'articles.status' => 'active',
			'article_tags.tagname' => $slug
		); 
		$data_query['order_by'] = array('date_of_action' => 'desc' );		
		$limit = 9;
		$offset = $limit * ($page_no - 1);
		$data_query['limit'] = $limit;
    	$data_query['offset'] = $offset;	

    	$this->standard_model->set_query_data($data_query);
		$data_results = $this->standard_model->select();
		if(is_object($data_results)) {
			$data_results = array($data_results);
		}
		$data['articles'] = $data_results;
		$this->load->helper('query');
		$data['total_pages'] = get_total_pages($data_query, $limit);
		$data['footer_articles'] = $this->footer_articles();

		$this->load->view('master',$data);			

		
	}

	private function get_category_details($slug='') {
		if($slug=='') {
			return; 
		}

		$data_query['table'] 	= 'categories';
		$data_query['field']	= 'id,name, meta_title, meta_description, keywords';
		$data_query['limit']	= 1;
		$data_query['condition'] = array(
			'status' => '1',
			'cat_slug' => $slug
		);				
		
		$this->standard_model->set_query_data($data_query);
		$data_results = $this->standard_model->select();

		return $data_results;
	}

	public function about_us(){
        $data['meta'] = meta();
        $data['meta_title'] = 'About Us';
		$data['meta_description'] = $data['meta']->meta_description;;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['meta_image'] = base_url() . 'assets/images/at-a-loss-uncertain-problem-depressed.jpg';
		
		$data['main_content'] = "about_view";
		$data['footer_articles'] = $this->footer_articles();
		$this->load->view('master',$data);
	}

	public function advertise(){
        $data['meta'] = meta();
        $data['meta_title'] = 'Advertise';
		$data['meta_description'] = $data['meta']->meta_description;;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['meta_image'] = '';
		
		$data['main_content'] = "advertise_view";
		$data['footer_articles'] = $this->footer_articles();
		$this->load->view('master',$data);
	}

	public function contact_us(){
        $data['meta'] = meta();
        $data['meta_title'] = 'Contact Us';
		$data['meta_description'] = $data['meta']->meta_description;;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['meta_image'] = base_url() .'assets/images/contact-us.jpg';
		$data['main_content'] = "contact_view";
		$data['footer_articles'] = $this->footer_articles();
		$this->load->view('master',$data);
	}

	public function faq(){
        $data['meta'] = meta();
        $data['meta_title'] = 'FAQ';
		$data['meta_description'] = $data['meta']->meta_description;;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['meta_image'] = base_url() .'assets/images/faq.jpg';
		
		$this->load->view('faq_view',$data);
	}

	public function terms_service() {
		$data['meta'] = meta();
		$data['meta_title'] = 'Tеrmѕ Of Service';
		$data['meta_description'] = $data['meta']->meta_description;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['meta_image'] = base_url() .'assets/images/terms-service.jpg';
		
		$this->load->view('terms_service_view',$data);
	}

	public function privacy_policy() {
		$data['meta'] = meta();
		$data['meta_title'] = 'Privacy Pоlісу';
		$data['meta_description'] = $data['meta']->meta_description;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		// $data['meta_image'] = base_url() .'assets/images/privacy-policy.jpg';
		$data['meta_image'] = '';

		$data['main_content'] = "privacy_policy_view";
		$data['footer_articles'] = $this->footer_articles();

		$this->load->view('master',$data);		
	}

	public function cookie_policy() {
		$data['meta'] = meta();
		$data['meta_title'] = 'Cookie Pоlісу';
		$data['meta_description'] = $data['meta']->meta_description;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['main_content']   = "cookie_policy_view";

		$this->load->view('master',$data);		
	}

	public function bad_ads() {
		$data['meta'] = meta();
		$data['meta_title'] = 'Bad Ads';
		$data['meta_description'] = $data['meta']->meta_description;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['main_content']   = "bad_ads_view";

		$this->load->view('master',$data);		
	}

	public function careers() {
		$data['meta'] = meta();
		$data['meta_title'] = 'Careers';
		$data['meta_description'] = $data['meta']->meta_description;
		$data['meta_keywords'] = $data['meta']->meta_keywords;
		$data['meta_image'] = base_url() .'assets/images/careers.jpg';

		$this->load->view('careers_view',$data);
	}
	
}
