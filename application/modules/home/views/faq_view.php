<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo $meta_title;?></title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel="alternate" type="application/rss+xml" title="<?php echo $meta_title;?>" />
    <meta name="description" content="<?php echo $meta_description;?>">
    <meta name="keywords" content="<?php echo $meta_keywords;?>">
    
    <link rel="canonical" href="<?php echo base_url(); ?>">
     <meta property="og:locale" content="en_US">
    <meta property="og:site_name" content="Market Capsule">
    <meta property="og:title" content="<?php echo $meta_title;?>">
    <meta property="og:url" content="<?php echo base_url(); ?>">
    <meta property="og:type" content="website">
    <meta property="og:description" content="<?php echo $meta_description;?>">
    <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.png" />
    <meta property="og:image" content="<?php echo $meta_image;?>" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <!-- Google+ / Schema.org -->
    <meta itemprop="name" content="Market Capsule">
    <meta itemprop="headline" content="<?php echo $meta_title;?>">
    <meta itemprop="description" content="<?php echo $meta_description;?>">
    <!-- To solve: The attribute publisher.itemtype has an invalid value -->
    <!-- Twitter Cards -->
    <meta name="twitter:title" content="<?php echo $meta_title;?>">
    <meta name="twitter:url" content="<?php echo base_url(); ?>">
    <meta name="twitter:description" content="<?php echo $meta_description;?>">
    <meta name="twitter:image" content="<?php echo $meta_image;?>" />
                <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162270014-3"></script>
        <script>window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-162270014-3');</script>   
        
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=PT+Serif:700&display=swap');
        @font-face { font-family: Gilroy; src: url("<?php echo base_url(); ?>assets/fonts/Gilroy-SemiBold.otf") format("opentype"); }
        body{
            font-family: "Gilroy",serif;
            background: #fff;
        }
        h4,h5{
            font-family: 'PT Serif', serif;
        }
        @media only screen and (min-width: 960px) {
            .container{    max-width: 920px;}   
            a{ outline: none; text-decoration: none!important; }
        }
    </style>    
</head>
<body class="pl-2 pr-2">    
    <section class="mb-4 mt-3 container">
        <div class="section-head pb-3 pt-3 align-middle">
            <a class="back-btn" href="<?php echo base_url();?>" style="position: relative; top: -2px; padding: 15px 0;">
                <svg width="22" height="22" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="10" cy="10" r="10" fill="#C0C8D2"/>
                <path d="M8.35226 9.96017L12.8097 5.50249C12.9325 5.38004 13 5.21633 13 5.04177C13 4.8671 12.9325 4.70349 12.8097 4.58085L12.4192 4.19045C12.2966 4.06762 12.1328 4 11.9582 4C11.7837 4 11.6201 4.06762 11.4974 4.19045L6.19007 9.4977C6.06694 9.62073 5.99952 9.78512 6 9.95988C5.99952 10.1354 6.06684 10.2996 6.19007 10.4227L11.4925 15.7248C11.6151 15.8477 11.7787 15.9153 11.9534 15.9153C12.128 15.9153 12.2916 15.8477 12.4143 15.7248L12.8048 15.3345C13.0589 15.0804 13.0589 14.6667 12.8048 14.4127L8.35226 9.96017Z" fill="#000D3A"/>
                </svg>
            </a>
        
            <span class="head-title ml-3">FAQ</span>
        </div>

        <div class="row mt-3">
            <div class="col-32 col-md-12">
                <img src="<?php echo base_url()?>assets/images/faq.jpg" style="width: 100%">
            </div>

            <div class="mt-3 col-12">
                <!-- <h4 class="text-center mt-3 title">FAQ</h4> -->

                <h5 class="mt-4 mb-1">Who are we</h5>
                <p>We are a team of passionate content creators herein to entertain you & bring smiles to your face.</p>

                <h5 class="mt-4 mb-1">What we do</h5>
                <p>We create & distribute content through our website lyricskr.com & our social media channels.</p>

                <h5 class="mt-4 mb-1">Values we believe in</h5>
                <p>Freedom of expression, Equality, Coexistence, Peace & Entertainment</p>

                <h5 class="mt-4 mb-1">Areas we pay attention to</h5>
                <p>We create content around Fashion, Lifestyle, Food, Technology, Viral News & Updates, Travel, Humor, Relationship, Sports, Marketing Updates and anything on Earth & beyond to entertain you.</p>

                <h5 class="mt-4 mb-1">How can you contribute to the content</h5>
                <p>If you are a content freak & your heart revolves around voicing your opinion out about anything & everything then you are at the right place! Share your best samples at hello@marketcapsule.in</p>

                <h5 class="mt-4 mb-1">Our Social Reach</h5>
                <span>We are to entertain you at:</span>
                <ul>
                    <li>Facebook</li>
                    <li>Instagram</li>
                    <li>Twitter</li>
                    <li>Youtube</li>
                    <li>Tiktok</li>
                </ul>

                <h5 class="mt-4 mb-1">Reach Us</h5>
                <p>Advertise with us or write to us at hello@marketcapsule.in</p>                
            </div>            
        </div>          
    </section>
</body>
</html>