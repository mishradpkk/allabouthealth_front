<style type="text/css">
@media (max-width: 480px) {
    .jeg_main .jeg_main_content {
        padding-right: 0;
        padding-left: 0;
    }
    .jeg_main .entry-header,
    .content-inner p{
        padding-right: 0px;
        padding-left: 0px;
    }     
}
</style>
<div class="jeg_main">
    <div class="jeg_container">
        <div class="jeg_content jeg_singlepage">
            <div class="container">
                <div class="row">
                    <div class="jeg_main_content col-md-12">
                        <div class="entry-header">
                            <h1 class="jeg_post_title" style="padding-top:20px; ">Advertise With Us</h1>
                        </div>
                        <div class="jeg_share_top_container"></div>
                        <div class="entry-content">
                            <div class="content-inner">
                                <p>
                                    Being in the field for such a lengthy period of time, we&#8217;ve realised one thing, quality of quantity is absolutely key. Our main goal is to ensure we don&#8217;t clutter our content with
                                    advertisements to ensure advertisers get fair exposure to audience.
                                </p>
                                <p>
                                    We believe we&#8217;re too early into our journey to start speaking to new advertisers, we want to understand what we&#8217;re able to offer first. Once we have collated adequate data we&#8217;ll open up
                                    to requests. It&#8217;s an exciting thought.
                                </p>
                                <figure class="wp-block-image alignwide size-large">
                                    <img
                                        src="<?php echo base_url();?>assets/images/at-a-loss-uncertain-problem-depressed.jpg"
                                        alt=""
                                        class="wp-image-251"                                       
                                    />
                                </figure>
                                <div class="jeg_share_bottom_container"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
