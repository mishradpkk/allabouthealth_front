<div id="propertySliderModal" class="modal" role="dialog" >
    <div class="modal-dialog modal-full">
        <!-- Modal content-->
        <div class="modal-content ">
            <div class="modal-header row">
                <div class="col-10">
                    <div class="header-block header-fashionology">
                        <div class="property small fashionology">
                            <img src="<?php echo base_url();?>assets/images/properties/fashionology.png" style="height: 55px;">   
                        </div>                        
                        <h3 class="modal-title text-left">Fashionology</h3>
                    </div>
                    <div class="header-block header-atu">
                        <div class="property small atu">
                            <img src="<?php echo base_url();?>assets/images/properties/ATU.png" style="height: 55px;">   
                        </div>                        
                        <h3 class="modal-title text-left">Amazing Things In The Universe</h3>
                    </div>
                    <div class="header-block header-the-nerd">
                        <div class="property small the-nerd" style="background: #fff;">
                            <img src="<?php echo base_url();?>assets/images/properties/the-nerd.png" style="height: 55px;">   
                        </div>                        
                        <h3 class="modal-title text-left">The Nerd</h3>
                    </div>
                    <div class="header-block header-ott-nation">
                        <div class="property small ott-nation">
                            <img src="<?php echo base_url();?>assets/images/properties/ott-nation.png" style="height: 80px; left: -10px;">   
                        </div>                        
                        <h3 class="modal-title text-left">OTT Nation</h3>
                    </div>
                    <div class="header-block header-tft">
                        <div class="property small tft">
                            <img src="<?php echo base_url();?>assets/images/properties/tft.png" style=" height: 55px;">   
                        </div>                        
                        <h3 class="modal-title text-left">The Filtered Tales</h3>
                    </div>
                    <div class="header-block header-marketing-moves">
                        <div class="property small marketing-moves">
                            <img src="<?php echo base_url();?>assets/images/properties/marketing-moves.png" style="height: 55px;">   
                        </div>                        
                        <h3 class="modal-title text-left">Marketing Moves</h3>
                    </div>
                    <div class="header-block header-ncr-feed">
                        <div class="property small ncr-feed">
                            <img src="<?php echo base_url();?>assets/images/properties/NCR-Feed.png" style="height: 55px;">   
                        </div>                        
                        <h3 class="modal-title text-left">NCR Feed</h3>
                    </div>
                    <div class="header-block header-mera-truck">
                        <div class="property small mera-truck">
                            <img src="<?php echo base_url();?>assets/images/properties/mera-truck.png" style="height: 55px;">   
                        </div>                        
                        <h3 class="modal-title text-left">Mera Truck Mera Jahan</h3>
                    </div>
                </div>
                <div class="col-2">
                    <button type="button" class="close modal-close" data-dismiss="modal">
                        <!-- <i class="fa fa-times"></i> -->
                        <svg width="24" height="24" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="10" cy="10" r="10" fill="#c0c8d2"/>
                        <path d="M11.084 9.85946L13.9024 7.02703C14.0325 6.8973 14.0325 6.7027 13.9024 6.57297L13.4688 6.11892C13.3388 5.98919 13.1436 5.98919 13.0136 6.11892L10.1734 8.95135C10.0867 9.03784 9.95664 9.03784 9.86992 8.95135L7.02981 6.0973C6.89973 5.96757 6.70461 5.96757 6.57453 6.0973L6.11924 6.55135C5.98916 6.68108 5.98916 6.87568 6.11924 7.00541L8.95935 9.83784C9.04607 9.92432 9.04607 10.0541 8.95935 10.1405L6.09756 12.9946C5.96748 13.1243 5.96748 13.3189 6.09756 13.4486L6.55285 13.9027C6.68293 14.0324 6.87805 14.0324 7.00813 13.9027L9.84824 11.0703C9.93496 10.9838 10.065 10.9838 10.1518 11.0703L12.9919 13.9027C13.122 14.0324 13.3171 14.0324 13.4472 13.9027L13.9024 13.4486C14.0325 13.3189 14.0325 13.1243 13.9024 12.9946L11.084 10.1622C10.9973 10.0757 10.9973 9.94595 11.084 9.85946Z" fill="white"/>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="modal-body">         
                <div class="resSlideShow">
                    <div class="modal-loader text-center">
                        <div class="showbox">
                            <div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>
                            <div class="loaderMessage"><h4>Not Available!</h4></div>
                        </div>
                    </div>
                    <div id="propertySlideshow" class="slideshow carousel slide mx-auto" data-ride="carousel" style="display: none">
                        <!-- The slideshow -->
                        <div class="carousel-inner"></div>

                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#propertySlideshow" data-slide="prev" role="button" >
                            <span aria-hidden="true">
                                <svg width="28" height="28" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="12.5" cy="12.5" r="12.5" fill="#C0C8D2" fill-opacity="0.6"/>
                                <path d="M10.1641 13.396L13.604 16.8358C13.8228 17.0547 14.1775 17.0547 14.3963 16.8358C14.615 16.6171 14.615 16.2623 14.3963 16.0436L11.3525 12.9999L14.3962 9.95638C14.6149 9.73756 14.6149 9.38284 14.3962 9.16411C14.1774 8.9453 13.8227 8.9453 13.6039 9.16411L10.164 12.6039C10.0546 12.7133 10 12.8566 10 12.9999C10 13.1433 10.0547 13.2867 10.1641 13.396Z" fill="black"/>
                                </svg>
                            </span>
                        </a>
                        <a class="carousel-control-next" href="#propertySlideshow" data-slide="next" role="button" >
                            <span aria-hidden="true">
                                <svg width="28" height="28" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="12.5" cy="12.5" r="12.5" fill="#C0C8D2" fill-opacity="0.6"/>
                                <path d="M15.3962 12.604L11.9564 9.16419C11.7375 8.94527 11.3828 8.94527 11.164 9.16419C10.9453 9.38292 10.9453 9.73768 11.164 9.95639L14.2078 13.0001L11.1641 16.0436C10.9454 16.2624 10.9454 16.6172 11.1641 16.8359C11.3829 17.0547 11.7376 17.0547 11.9564 16.8359L15.3963 13.3961C15.5057 13.2867 15.5603 13.1434 15.5603 13.0001C15.5603 12.8567 15.5056 12.7133 15.3962 12.604Z" fill="black"/>
                                </svg>
                            </span>
                        </a>                    
                    </div>
                </div>

                <div class="properties mt-2 mb-4 row">
                    <div class="col col-md-12 slide-sec">
                        <ul class="slide-bar text-center">
                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='fashionology' >
                                    <div class="groupBlock">
                                        <div class="property fashionology">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/fashionology.png" style="height: 64px;">
                                        </div>
                                        <p class="text-center">Fashionology</p>
                                    </div>
                                </a>                                
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='atu'>
                                    <div class="groupBlock">
                                        <div class="property atu">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/ATU.png" style="height: 62px;">
                                        </div>
                                        <p class="text-center">Amazing Things In The Universe</p>
                                    </div>
                                </a>
                            </li>       
                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='the-nerd'>
                                    <div class="groupBlock">
                                        <div class="property the-nerd" style="background: #fff;">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/the-nerd.png" style="height: 64px;">
                                        </div>
                                        <p class="text-center">The Nerd</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='ott-nation'>
                                    <div class="groupBlock">
                                        <div class="property ott-nation">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/ott-nation.png">
                                        </div>
                                        <p class="text-center">OTT Nation</p>
                                    </div>
                                </a>                                
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='tft'>
                                    <div class="groupBlock">
                                        <div class="property tft">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/tft.png">
                                        </div>
                                        <p class="text-center">The Filtered Tales</p>
                                    </div>
                                </a>
                            </li>           

                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='marketing-moves'>
                                    <div class="groupBlock">
                                        <div class="property marketing-moves">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/marketing-moves.png" style="height: 75px;">
                                        </div>
                                        <p class="text-center">Marketing Moves</p>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='ncr-feed'>
                                    <div class="groupBlock">
                                        <div class="property ncr-feed">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/NCR-Feed.png" style="height: 64px;">
                                        </div>
                                        <p class="text-center">NCR Feed</p>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="propertyModalBtn" data-property='mera-truck'>
                                    <div class="groupBlock">
                                        <div class="property mera-truck">
                                            <img src="<?php echo base_url(); ?>assets/images/properties/mera-truck.png" style="height: 75px;">
                                        </div>
                                        <p class="text-center">Mera Truck Mera Jahan</p>
                                    </div>
                                </a>
                            </li>
                                                
                        </ul>                   
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>