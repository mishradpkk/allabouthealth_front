<style type="text/css">
@media (max-width: 480px) {
    .jeg_main .jeg_main_content {
        padding-right: 0;
        padding-left: 0;
    }
    .jeg_main .entry-header,
    .content-inner p{
        padding-right: 0px;
        padding-left: 0px;
    }     
}
</style>
<div class="jeg_main">
    <div class="jeg_container">
        <div class="jeg_content jeg_singlepage">
            <div class="container">
                <div class="row">
                    <div class="jeg_main_content col-md-12">
                        <div class="entry-header" style="padding-top: 20px">
                            <h1 class="jeg_post_title">Contact Us</h1>
                        </div>
                        <div class="jeg_share_top_container"></div>
                        <div class="entry-content">
                            <div class="content-inner">
                                <div class="row vc_row">
                                    <div class="jeg-vc-wrapper">
                                        <div class="wpb_column jeg_column vc_column_container vc_col-sm-12 jeg_main_content">
                                            <div class="jeg_wrapper wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <p>
                                                            Far far away, probably thousands of miles away from you, our team are working hard to keep this site going. Content doesn&#8217;t just grow on trees sadly. Thanks for opting to get
                                                            in touch, we can&#8217;t wait to hear from you.
                                                        </p>

                                                        <p>Share your feedback on <a href="mailto: theviralstreetusa@gmail.com" style=" text-decoration-line: underline!important; color: #1f308a;">theviralstreetusa@gmail.com</a></p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="row vc_row">
                                    <div class="jeg-vc-wrapper">
                                        <div class="wpb_column jeg_column vc_column_container vc_col-sm-12 jeg_main_content">
                                            <div class="jeg_wrapper wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <p>We&#8217;ll aim to respond to you within 2 working days.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jeg_share_bottom_container"></div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>        
    </div>
</div>
