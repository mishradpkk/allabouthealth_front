<style type="text/css">
body{
    overflow-x: hidden; 
}
.css-ehrc55 {
    line-height: 0;
    margin: 0 auto;
    width: 100%;
    max-width: 1500px;
}
.css-icas44 {
    position: static;
    width: 100vw;
    display: flex;
    justify-content: center;
    min-height: 0;
    max-width: 1500px;
    margin: 40px 0 60px 0;
}
.css-9ovz5l {
    text-align: center;
    display: flex;
    justify-content: center;
    flex-direction: column;
    height: unset;
    color: #231f20;
    max-width: 550px;
    min-height: unset;
    width: calc(100vw - 40px);
}

.css-9ovz5l h1 {
    font-size: 42px;
    line-height: 48px;
    margin: 5px 0 10px 0;
    color: #231f20;
}
.css-1jisqi6 {
    margin-top: 0;
}

.css-9ovz5l p {
    margin: 0;
    font-size: 22px;
    line-height: 30px;
    font-family: inherit;
    color: #231f20;
}
.css-stl7tm {
    margin-left: auto;
    margin-right: auto;
    max-width: 550px;
    width: calc(100vw - 40px);
}
.css-1p72dae {
    margin-bottom: 60px;
}
.css-5qz12o {
    margin-bottom: 40px;
}
.css-1advikf {
    padding-bottom: 15px;
    border-bottom: solid 1px #dcdbdb;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
}
.css-1vldhk5 {
    color: inherit;
    margin-bottom: 0;
    margin-top: 0;
    text-transform: uppercase;
    font-family: "Proxima Nova Condensed",system-ui,sans-serif;
    font-weight: 700;
    font-size: 20px;
    line-height: 26px;
}
.css-stl7tm {
    margin-left: auto;
    margin-right: auto;
    max-width: 550px;
    width: calc(100vw - 40px);
}
.css-1h1rean {
    display: block;
    justify-content: space-between;
    width: 100%;
}
.css-z468a2 {
    width: 100%;
    max-width: 550px;
    position: relative;
    margin: 0 auto;
}
.css-17mrx6g {
    padding: 0;
    margin: 0;
    list-style: none;
}
.css-iyua43 {
    flex: 1;
    max-width: 100%;
    margin-bottom: 40px;
}
.css-fnd80r {
    text-align: center;
    font-size: 0;
    line-height: 0;
    flex: none;
    margin: 0 0 20px;
    margin-bottom: 15px;
}
.css-yvmi2y {
    cursor: pointer;
    text-decoration: none;
    border-color: currentColor;
    color: #01adb9;
    color: inherit;
}
.css-1f8zf1f {
    width: 100%;
    display: block;
    text-align: center;
    font-size: 0;
    line-height: 0;
    outline: none;
    position: relative;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
    z-index: 0;
}
.css-rlaxw5 {
    display: block;
    position: relative;
    padding-bottom: 75%;
}
.css-1u8c2i8 {
    width: 100%;
    height: 185px;
    object-fit: cover;
}
.css-1ko2l1b {
    cursor: pointer;
    text-decoration: none;
    border-color: currentColor;
    color: #01adb9;
    color: inherit;
    font-family: "Proxima Nova Condensed",system-ui,sans-serif;
    font-weight: 700;
    text-decoration: none;
    display: block;
    color: #231f20;
    margin: 0;
    font-size: 30px;
    line-height: 34px;
    margin-bottom: 15px;
}

.css-19z6m27 {
    margin: 10px 0 20px 0;
    font-size: 17px;
    line-height: 26px;
    margin-top: 15px;
    margin-bottom: 30px;
}
.css-2fdibo {
    cursor: pointer;
    text-decoration: none;
    color: inherit;
}
.css-1xfwvje {
    cursor: pointer;
    text-decoration: none;
    border-color: currentColor;
    color: #01adb9;
    font-size: 16px;
    font-weight: 600;
    text-align: center;
    margin: 0 auto 20px;
    margin-top: 30px;
    margin-bottom: 0;
}
.css-1xfwvje {
    display: none;
    margin-top: 20px;
}
.css-15xqzls {
    margin-left: 7px;
    position: relative;
    top: 1px;
}
@media (min-width: 768px) {
    .css-icas44 {
        margin: 35px 0 72px 0;
    }    
    .css-9ovz5l {
        max-width: 625px;
        width: calc(100vw - 90px);
    }
    .css-9ovz5l h1 {
        font-size: 70px;
        line-height: 74px;
        margin: 10px 0 15px 0;
    }
    .css-9ovz5l p {
        font-size: 28px;
        line-height: 36px;
    }
    .css-stl7tm {
        max-width: 989px;
        width: calc(100vw - 110px);
    }
    .css-1p72dae {
        margin-bottom: 70px;
    }
    .css-5qz12o {
        margin-bottom: 50px;
    }
    .css-1advikf {
        padding-bottom: 20px;
        flex-direction: row;
    }
    .css-1vldhk5 {
        font-size: 24px;
    }
    .css-stl7tm {
        max-width: 989px;
        width: calc(100vw - 110px);
    }
    .css-z468a2 {
        max-width: 879px;
    }
    .css-iyua43 {
        margin-bottom: 50px;
    }
    .css-lm0pdr {
        display: flex;
    }
    .css-fnd80r {
        margin-bottom: 0;
        width: 150px;
        margin-right: 30px;
    }
    .css-1ko2l1b {
        font-size: 34px;
        line-height: 38px;
        margin-bottom: 10px;
    }
    .css-19z6m27 {
        font-size: 16px;
        line-height: 22px;
        margin-top: 0;
        margin-bottom: 0;
    }
}
@media (min-width: 990px){
    .css-icas44 {
        margin: 40px 0 70px 0;
    }
    .css-9ovz5l {
        max-width: 750px;
    }
    .css-stl7tm {
        max-width: 1100px;
        width: calc(100vw - 90px);
    }
    .css-stl7tm {
        max-width: 1100px;
        width: calc(100vw - 90px);
    }
    .css-1h1rean {
        display: flex;
    }
    .css-z468a2 {
        width: calc(100% - 350px);
        max-width: 750px;
    }
    .css-fnd80r {
        width: 200px;
        margin-right: 25px;
    }
    .css-1ko2l1b {
        margin-bottom: 15px;
    }
}
@media (min-width: 1190px) {
    .css-fnd80r {
        width: 245px;
    }
}

@media (max-width: 767px) {

    .css-1xfwvje {
    display: block;
    font-size: 14px;
    padding: 9px 18px 10px 16px;
    width: 210px;
    color: white;
    background: #e0218e;
    padding: 0;
    line-height: 50px;
    height: 50px;
}
    .css-15xqzls {
        display: none;
    }


}

</style>
<div class="css-ehrc55">
    <div class="css-icas44">
        <div class="css-9ovz5l">
            
            <h1 class="css-1jisqi6"><?php echo $category->name; ?></h1>
            <p class="css-1rnzyga"><?php echo $category->about; ?></p>
        </div>
    </div>
</div>
<div class="css-stl7tm">
    <div class="css-1p72dae">
        <div class="css-5qz12o">
            <div class="css-1advikf"><h2 class="css-1vldhk5"></h2></div>
        </div>

        <div class="css-stl7tm">
            <div class="css-1h1rean">
                <div class="css-z468a2">
                    <ul class="css-17mrx6g">
                        <?php foreach ($latest_articles as $article) { ?>
                        <li class="css-iyua43">
                            <div class="css-lm0pdr">
                                <div class="css-fnd80r">
                                    <a class="css-yvmi2y" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>" >
                                        <img src="<?php echo CDN_IMAGE_URL . $article->id . '/'. $article->cover_image;?>" alt="" class="css-1u8c2i8" />
                                    </a>
                                </div>
                                <div>
                                    <a class="css-1ko2l1b" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                                        <?php echo $article->title; ?>
                                    </a>
                                    <p class="css-19z6m27">
                                        <a class="css-2fdibo" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>" aria-hidden="true" >
                                            <?php echo $article->short_description; ?>
                                        </a>
                                    </p>
                                    <a class="css-0 css-1xfwvje" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                                        READ MORE<span class="css-15xqzls icon-hl-arrow-right css-0"></span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <section class="css-82r5pg">
                    <div class="css-1d3cz3a">
                        <div class="css-1hjaa0k"><span class="css-7mq29v bt-uid-tg" data-uid="5ac2688e0d-160"></span></div>
                        
                    </div>
                </section>
            </div>
        </div>
    </div>
  
</div>
