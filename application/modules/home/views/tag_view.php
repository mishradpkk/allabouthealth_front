<style type="text/css">
@media (max-width: 480px) {
    .jeg_main .jeg_main_content {
        padding-right: 0;
        padding-left: 0;
    }        
}
</style>
<div class="jeg_main">
    <div class="jeg_container">
        <div class="jeg_content">
            <div class="jeg_section">
                <div class="container">
                    <div class="jeg_cat_content row">
                        <div class="jeg_main_content col-sm-12">
                            <div class="jeg_inner_content">
                                <div class="jeg_archive_header" style="padding-top: 20px; ">
                                    <h1 class="jeg_archive_title">Tag: <?php echo $tag_name; ?></h1>
                                </div>

                                <?php if(sizeof($articles) ==0) { ?>
                                <div class="jnews_archive_content_wrapper">
                                    <div class="jeg_postblock_3 jeg_postblock jeg_col_2o3">
                                        <div class="jeg_block_container">
                                            <div class="jeg_empty_module">No Content Available</div>
                                        </div>
                                    </div>
                                </div>
                                <?php } else { ?>

                                <div class="jeg_postblock_11 jeg_postblock jeg_module_hook jeg_pagination_nav_1 jeg_col_3o3 jnews_module_4651_1_5ec3c52c08d53" data-unique="jnews_module_4651_1_5ec3c52c08d53">
                                        <div class="jeg_block_container">
                                            <div class="jeg_posts_wrap">
                                                <div class="jeg_posts jeg_load_more_flag">
                                                <?php foreach ($articles as $article) { ?>
                                                <article class="jeg_post jeg_pl_md_card format-standard">
                                                    <div class="jeg_inner_post">
                                                        <div class="jeg_thumb">
                                                            <a href="<?php echo base_url() . $article->slug; ?>">
                                                                <div class="thumbnail-container size-715">
                                                                    <img
                                                                        width="350"
                                                                        height="250"
                                                                        src="<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>"
                                                                        class="wp-post-image"
                                                                        alt=""
                                                                    />
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="jeg_postblock_content">
                                                            <div class="jeg_post_category">
                                                                <span>
                                                                    <a href="<?php echo base_url() . 'category/' . $article->cat_slug; ?>"><?php echo $article->category; ?></a>
                                                                </span>
                                                            </div>
                                                            <h3 class="jeg_post_title">
                                                                <a href="<?php echo base_url() . $article->slug; ?>"><?php echo $article->title; ?></a>
                                                            </h3>                                                            
                                                        </div>
                                                    </div>
                                                </article>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
