<?php foreach ($articles as $article) { ?>
<div class="col-6 col-md-4 col-sm-6 article">
    <article>
        <div class="cover-wrapper">
            <a href="<?php echo base_url() . $article->slug; ?>">
                <?php if(file_exists(UPLOAD_PATH.$article->id . '/' . $article->cover_image.'.webp')) { ?>
                    <img class="d-block w-100" src="<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>.webp" onerror="this.src='<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>';this.onerror='';" alt="<?php echo $article->title; ?>">
                <?php } else { ?>
                    <img class="d-block w-100" src="<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>" alt="<?php echo $article->title; ?>">
                <?php } ?>
                <div class="category-tag small">
                    <a href="<?php echo base_url() . $article->cat_slug; ?>"><?php echo $article->category; ?></a>
                </div>
            </a>
        </div>
        <div class="body-wrapper">
            <a href="<?php echo base_url() . $article->slug; ?>">
                <h3 class="title"><?php echo $article->title; ?></h3>
            </a>
        </div>                
    </article>
</div>
<?php } ?>