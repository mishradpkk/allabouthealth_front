<style type="text/css">
    /*.latest-stories .small-card p{
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
    }*/
</style>

<main class="main-content">

    <div class="section">
        
        <div class="section-header">
            <h2>Featured Topics</h2>
        </div>
        
        <div class="section-body">
            <ul class="four-block">
                <li>
                    <a href="<?php echo base_url();?>yoga">
                        <img src="<?php echo base_url();?>assets/images/yoga.jpg" />
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>weight-loss">
                        <img src="<?php echo base_url();?>assets/images/weight-loss.jpg" />
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>nutrition">
                        <img src="<?php echo base_url();?>assets/images/nutrition.jpg" />
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>exercise">
                        <img src="<?php echo base_url();?>assets/images/exercise.jpg" />
                    </a>
                </li>
            </ul>
        </div>
    </div>
    
    <!-- <div class="section" style="display: none">
        
    </div> -->

    <div class="section latest-stories">
        <div class="section-header">
            <h2>Latest Stories</h2>
        </div>
        <?php 
        // echo "<pre>";
        // print_r($latest_articles);
        // echo "</pre>";
        ?>
        <ul class="section-body">
            <li class="big-card">
                <div class="content">
                    <a class="title" href="<?php echo base_url(). $latest_articles[0]->cat_slug . '/'.$latest_articles[0]->slug; ?>">
                        <img alt="" src="<?php echo CDN_IMAGE_URL . $latest_articles[0]->id . '/'. $latest_articles[0]->cover_image;?>">
                    </a>
                    
                    <div>
                        <div class="category-name"><?php echo $latest_articles[0]->category; ?></div>
                        <a class="title" href="<?php echo base_url(). $latest_articles[0]->cat_slug . '/'.$latest_articles[0]->slug; ?>">
                            <?php echo $latest_articles[0]->title; ?>
                        </a>
                        <p>
                            <a href="<?php echo base_url(). $latest_articles[0]->cat_slug . '/'.$latest_articles[0]->slug; ?>">
                                <?php echo $latest_articles[0]->short_description; ?>
                            </a>
                        </p>
                        <a class="read-more" href="<?php echo base_url(). $latest_articles[0]->cat_slug . '/'.$latest_articles[0]->slug; ?>" >
                            READ MORE<span></span>
                        </a>
                    </div>
                </div>
            </li>
            <?php array_shift($latest_articles); foreach ($latest_articles as $article) { ?>
            <li class="small-card">
                <div class="content">
                    <figure>
                        <a href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                            <span class="first-span">
                                <span class="second-span" style="height: 140px;">
                                    <img alt="" src="<?php echo CDN_IMAGE_URL . $article->id . '/'. $article->cover_image;?>" style="height: 100%; object-fit: cover;" /> 
                                </span>
                            </span>
                        </a>
                    </figure>
                    <div>
                        <div class="category-name"><?php echo $article->category; ?></div>
                        <a class="title" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                            <?php echo $article->title; ?>
                        </a>
                        <p>
                            <a href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                                <?php echo $article->short_description; ?>
                            </a>
                        </p>
                        <a class="read-more" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>" >
                            READ MORE<span></span>
                        </a>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>

    <div class="section trending-stories" style="margin-bottom: 40px;">
        <div class="section-header">
            <h2>Trending</h2>
        </div>

        <div class="section-body">
            <div class="section-container">
                <div class="col-left">
                    <ul>
                        <?php foreach ($trending_articles as $article) { ?>
                        <li>
                            <div class="content">
                                <figure>
                                    <a href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                                        <span class="first-span">
                                            <span class="second-span">
                                                <img alt="" src="<?php echo CDN_IMAGE_URL . $article->id . '/'. $article->cover_image;?>" style="height: 100%; object-fit: cover;" /> 
                                            </span>
                                        </span>
                                    </a>
                                </figure>
                                <div>
                                    <!-- <div class="sponsored">Sponsored</div> -->
                                    <a class="title" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                                        <?php echo $article->title; ?>
                                    </a>
                                    <p>
                                        <a href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                                            <?php echo $article->short_description; ?>
                                        </a>
                                    </p>
                                    <a class="read-more" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>" >
                                        READ MORE<span class="icon-hl-arrow-right"></span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                        
                    </ul>
                </div>

                <section class="col-right">
                    <div>
                        <div>
                            <div class="nsl-block">
                                <div class="nsl-body">
                                    <h2>Stay on top of the COVID-19 pandemic</h2>
                                    <p>We’ll email you the latest developments about the novel coronavirus and Healthline's top health news stories, daily.</p>
                                    <div class="signup-block">
                                        <form action="/newsletter-signup" method="post" novalidate="">
                                            <div class="form-group"><input placeholder="Enter your email" type="email" /></div>
                                            <button type="submit">
                                                <span><span>SIGN UP NOW</span></span>
                                            </button>
                                        </form>
                                        <p>Your <a target="_blank" rel="noopener noreferrer" href="<?php echo base_url();?>privacy-policy">privacy</a> is important to us</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- <div class="section health-voices">
        <div class="section-header">
            <h2>Healthline Voices</h2>
            <a class="view-all" href="<?php echo base_url();?>program/healthlinevoices">
                View all
            </a>
        </div>
        <div class="section-body">
            <ul>
                <li>
                    <div class="title">Jamie Tworkowski</div>
                    <img alt="" src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2019-2/732x549_Jamie_Tworkowski.jpg?w=462" />
                    <ul>
                        <li>
                            <a class="css-cc8hw7" data-event="Landing Page Engagement|Link Click_Healthline Voices_Contributor's Widget_1|" href="<?php echo base_url();?>health/depression/twloha-suicide-prevention-day-interview">
                                To Write Love on Her Arms Founder Opens Up About His Own Mental Health Journey
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="title">Sara Zayed</div>
                    <img alt="" src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2019-2/732x549_Sara_Zayed.jpg?w=462" />
                    <ul>
                        <li>
                            <a class="css-cc8hw7" data-event="Landing Page Engagement|Link Click_Healthline Voices_Contributor's Widget_2|" href="<?php echo base_url();?>health/food-nutrition/plant-based-nutrition-fueling-workouts">
                                Can You Work Out and Still Maintain a Plant-Based Diet? Yes — Here’s How
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="title">Mila Buckley</div>
                    <img alt="" src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2019-2/732x549_Mila.jpg?w=462" />
                    <ul>
                        <li>
                            <a class="css-cc8hw7" data-event="Landing Page Engagement|Link Click_Healthline Voices_Contributor's Widget_3|" href="<?php echo base_url();?>health/type-2-diabetes/mila-clarke-buckley-type-2-diabetes-support-app">
                                When She Couldn’t Find the Type 2 Diabetes Support She Needed, Mila Clarke Buckley Began Helping Others Cope
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="title">Rachael DeVaux</div>
                    <img alt="" src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2019-2/732x549_RachaelDevaux.jpg?w=462" />
                    <ul>
                        <li>
                            <a class="css-cc8hw7" data-event="Landing Page Engagement|Link Click_Healthline Voices_Contributor's Widget_4|" href="<?php echo base_url();?>health/fitness-nutrition/post-workout-foods-hiit">
                                5 Delicious Foods to Refuel with After a HIIT Session
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div> -->
    
    <div class="section top-reads">
        <div class="section-header" style="margin-bottom: 30px;">
            <h2>More Top Reads</h2>
        </div>

        <ul class="tr-lists">
            
            <?php foreach ($more_top as $article) { ?>
            <li>
                <div class="content">
                    <figure class="css-ymgzhk">
                        <a href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                            <span class="first-span">
                                <span class="second-span" style="">
                                    <img alt="" src="<?php echo CDN_IMAGE_URL . $article->id . '/'. $article->cover_image;?>" style="height: 100%; object-fit: cover;" /> 
                                </span>
                            </span>
                        </a>
                    </figure>
                    <div>
                        <a class="title" href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                            <?php echo $article->title; ?>
                        </a>
                        <p class="css-16ywm1g" style="display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;">
                            <a href="<?php echo base_url(). $article->cat_slug . '/'.$article->slug; ?>">
                                <?php echo $article->short_description; ?>
                            </a>
                        </p>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>

    <!--<div  class="section spotlight">
        <div class="section-header">
            <h2>Spotlight</h2>
        </div>
        <div class="section-body">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>program/sexual-health-for-every-body">
                        <img src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2020-8/547679-HL-Sexual_Health_Awareness_Month-600x900-Pinterest.20200826190206859_0.jpg?w=630"/>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>health/video/rheumatoid-arthritis-mentors#1">
                        <img src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2021-1/Humira-Rheumatoid_Arthritis-600x900-Pinterest-01.png?w=630"/>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>health/mental-health/managing-your-mental-health-during-the-pandemic">
                        <img src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2020-8/Managing-Your-Mental-Health-During-the-Pandemic-600x900-SPOTLIGHT.jpg?w=630"/>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>health/breakup-grief">
                        <img src="//i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2020-4/9586-Grief_Series-_The_One_You_Lost_Podcast_on_Loss-04.20200422165425838.jpg?w=630"/>
                    </a>
                </li>
            </ul>
        </div>
    </div> -->

    <!-- <div class="section featured-topics">
        <div class="section-header">
            <h2>Featured Health Topics</h2>
            <a class="view-all" href="<?php echo base_url();?>featured-health-topics">
                View all
            </a>
        </div>
        <div class="section-body">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>program/get-to-the-heart-t2-diabetes">Get to the Heart of Type 2 Diabetes</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>program/no-rheum-for-ra-pain">No Rheum for RA Pain </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>program/beyond-back-pain-with-as">Beyond Back Pain with AS</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>program/more-than-skin-deep-with-psoriasis">More Than Skin Deep with Psoriasis</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>program/navigating-life-with-bipolar-disorder">Navigating Life with Bipolar Disorder</a>
                </li>
            </ul>
        </div>
    </div> -->
</main>
