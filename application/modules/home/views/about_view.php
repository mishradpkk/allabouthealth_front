<style type="text/css">
@media (max-width: 480px) {
    .jeg_main .jeg_main_content {
        padding-right: 0;
        padding-left: 0;
    }
    .jeg_main .entry-header,
    .content-inner p{
        padding-right: 0px;
        padding-left: 0px;
    }     
}
</style>
<div class="jeg_main jeg_sidebar_none">
    <div class="jeg_container">
        <div class="jeg_content jeg_singlepage">
            <div class="container">
                <div class="row">
                    <div class="jeg_main_content col-md-12">
                        <div class="entry-header" style="padding-top: 20px;">
                            <h1 class="jeg_post_title">About</h1>
                        </div>
                        <div class="jeg_share_top_container"></div>
                        <div class="entry-content">
                            <div class="content-inner">
                                <div id="idTextPanel" class="jqDnR">
                                    <p>
                                        We&#8217;re a group of social media experts who got seriously tired of one thing, copious amounts of advertisements with content spread across dozens of pages. Here at TheViralStreet we wanted to
                                        <strong>change that</strong>. Of course advertisements are crucial but we wanted to allow people to enjoy viral stories with a better user experience.
                                    </p>
                                    <p>
                                        Founded in 2020, it&#8217;s time explore new stories in a much more enjoyable way. You&#8217;ll probably find our links across all of your favourite social media sites, please enjoy them, our goal is
                                        to ensure that you want to come back again.
                                    </p>
                                    <p>
                                        When it comes to the content, our writers enjoy writing about pretty much anything, so you&#8217;ll definitely find something that will take your interest. We&#8217;re always interested to finding
                                        more passionate writers though, even better, <strong>you&#8217;ll get paid</strong>.
                                    </p>
                                    <p>
                                        We&#8217;ve always believed in &#8216;play hard, work hard&#8217;, all of our staff work from home allowing them to enjoy important times with their families. We don&#8217;t even have a dedicated
                                        office, maybe they will eventually become a thing of the past. Imagine if working from home became a reality for the majority, our planet would boom.
                                    </p>
                                    <p>
                                        2020 was a critical period for the world, we saw a world disaster which bought everybody together, it really shows that we&#8217;re all here for each other when we need people most. TheViralStreet was
                                        born to keep minds occupied during isolation.
                                    </p>
                                    <p>
                                        <em><strong>Happy reading &#8211; TheViralStreet.</strong></em>
                                    </p>
                                </div>
                                <div class="jeg_share_bottom_container"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
