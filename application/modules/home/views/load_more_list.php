<?php $index =0; foreach ($load_article['articles'] as $article) { ?>                    
    <article class="jeg_post jeg_pl_md_card format-standard">
        <div class="jeg_inner_post">
            <div class="jeg_thumb">
                <a href="<?php echo base_url() . $article->slug; ?>">
                    <div class="thumbnail-container size-715">
                        <img
                            width="350"
                            height="250"
                            src="<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>"
                            class="wp-post-image"
                            alt=""
                        />
                    </div>
                </a>
            </div>
            <div class="jeg_postblock_content">
                <div class="jeg_post_category">
                    <span>
                        <a href="<?php echo base_url() . 'category/' . $article->cat_slug; ?>"><?php echo $article->category; ?></a>
                    </span>
                </div>
                <h3 class="jeg_post_title">
                    <a href="<?php echo base_url() . $article->slug; ?>"><?php echo $article->title; ?></a>
                </h3>                                                            
            </div>
        </div>
    </article>
<?php $index++; } ?>