<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller {

	public function __construct()
	{
		 parent::__construct();		 
		 $this->load->model('standard_model');
	}


	public function index_get() {			
		redirect(base_url());
	}

	public function article_get($article_id =0) {
		if($article_id!=0) {
			$data['article'] = $this->article_description_id($article_id);
			$data['main_content'] = "article_view";
			if(isset($data['article']->id)) {
				$this->load->helper('settings'); 
				$this->load->helper('ads'); 
		 		$this->load->helper('query');

				$data['layout'] = 'single';
				$data['page_param'] = 1;
				$data['load'] = 'dynamic';
				$data['meta'] = meta();
				$data['next_article'] = get_next_story($data['article']->id, $data['article']->language);
				
				$article_media = $this->article_media($article_id);
				$adindex = 2;

				$meta = meta(); 
				$ads = page_ads();

				$view_data['article'] = array(
					'id' => $data['article']->id, 
					'title' => $data['article']->title, 
					'url' => base_url() . $data['article']->slug,					
				);

				// $view_data['article']['ads']['anchor'] = $ads['anchor'];
				// $view_data['article']['ads']['ad1'] = $ads['ad1'];
				// $view_data['article']['ads']['ad2'] = $ads['ad2'];

				$index = 0;
				foreach ($article_media as $media) {					
					$res_data['res'] = $media;
					$res_data['index'] = $index+1;

					if($adindex>2) {
						$adindex = 1;
					}
					$res_data['adindex'] = $adindex;
					$res_data['index'] = $index;

					$view_data['media'][$index]['html'] = $this->load->view('article/media_block',$res_data, true);
					$view_data['media'][$index]['adsId'] = 'ads_'.$media->post_id.'_'.$media->id;

					// $view_data['media'][$index]['ads'] = $ads['ad'.$adindex];
					$view_data['media'][$index]['ads'][0] = $ads['banner_ad1'];
					$view_data['media'][$index]['ads'][1] = $ads['banner_ad2'];
					$view_data['media'][$index]['adindex'] = $adindex;
					$view_data['media'][$index]['index'] = $index;
					$adindex++;
					$index++;
				}

				/*$more_data['article_id'] = $data['article']->id;
				$more_data['editors_pick_articles'] = you_may_also_like($data['article']->id, $data['article']->id, $data['article']->language);
				$view_data['editors_pick'] = $this->load->view('article/editors_pick_view',$more_data, true);				

				$view_data['next_article'] = array(
					'id' => $data['next_article']->id, 
					'title' => $data['next_article']->title, 
					'url' => base_url() . $data['next_article']->slug,
				);

				$view_data['prev_article'] = array(
					'id' => $data['next_article']->id, 
					'title' => $data['next_article']->title, 
					'url' => base_url() . $data['next_article']->slug,
				);*/

			}

			$this->response(array('status'=>true, 'data'=>$view_data));			
		}
	}

	private function article_description_id($article_id="") {

		if ($article_id == "") {
			return false;
		} 

		$data['table'] 	= 'articles';
		$data['field']	= 'articles.id,title,articles.cat_id,short_description,articles.cover_image,slug,articles.tags,articles.description, date_of_creation,date_of_action,categories.name as cat_name, categories.cat_slug, articles.language';
		$data['join']	= array(
				'categories' => 'articles.cat_id=categories.id', 
			);
		$data['condition'] = array(		
			'articles.status' => 'active',
			'articles.id' => $article_id
		);
		$data['limit'] = 1;
		$this->standard_model->set_query_data($data);
		$data_results = $this->standard_model->select();

		return $data_results;		
	}

	private function article_media($article_id=0, $limit=0, $offset=0, $current_page = 0) {

		if ( $article_id <= 0) {
    		return;
    	}

    	$data['table'] 		= 'media';
		$data['field'] 		= 'id,post_id,media,source,caption,description,doc, media_type, alt_attribute, height, width';
    	$data['condition'] 	=  array( 'post_id' => $article_id, 'status' => '1');
	    $data['order_by']   = array('media_order' => 'ASC');

	    // $data['limit']  = 2;
	    if ($limit >0) {
	    	$data['limit']  = $limit;
    		$data['offset'] = $offset;		    	
	    }

    	$this->standard_model->set_query_data($data);
		$data_results = $this->standard_model->select();

		if(is_object($data_results)) {
			$data_results = array($data_results);
		} 

		return $data_results;		
	}

	public function property_images_post() {
		$property = $this->input->post('property');
		if($property == 'fashionology') {
			$property_id = 1;
		} else if($property == 'atu'){
			$property_id = 2;
		} else if($property == 'bollywood-moves'){
			$property_id = 3;
		} else if($property == 'ott-nation'){
			$property_id = 4;
		} else if($property == 'tft'){
			$property_id = 5;
		} else if($property == 'marketing-moves'){
			$property_id = 6;
		} else if($property == 'ncr-feed'){
			$property_id = 7;
		} else if($property == 'mera-truck'){
			$property_id = 8;
		} else if($property == 'the-nerd'){
			$property_id = 9;
		} else {
			$this->response(array('status' => false, 'message'=>'Invalid Property'));
		}

		$data_query['table'] 		= 'property_images';
		$data_query['field'] 		= 'image_url as image';
    	$data_query['condition']	=  array( 'property_id' => $property_id, 'status' => '1');
	    $data_query['order_by']   	= array('id' => 'desc');

	    $limit = 10;
	    $offset = 0;
	    if($this->input->post('limit')) {
	    	$limit = $this->input->post('limit');
	    }

	    if($this->input->post('offset')) {
	    	$offset = $this->input->post('offset');
	    }

    	$data_query['limit']  = $limit;
		$data_query['offset'] = $offset;

		$this->standard_model->set_query_data($data_query);
		$data_results = $this->standard_model->select();

		if(is_object($data_results)) {
			$data_results = array($data_results);
		} 
	    
		$this->response(array('status' => true, 'data'=> $data_results));
	}
		
	public function notification_user_post(){
		$user = $this->input->post('user');
		
		$data_query['table'] = 'notification_users';
        $data_query['field'] = 'id';
		
		if($this->is_valid_email($user)) {
			$insert_data['email'] = $user;
        	$data_query['condition']['email'] = $user;
		} else if($this->is_validate_mobile($user)) {
			$insert_data['mobile'] = $user;
        	$data_query['condition']['mobile'] = $user;
		} else {
			$this->response(array('status' => false, 'message'=> "Please enter valid details." ));	
		}

        $data_query['limit'] = 1;
        
        $this->standard_model->set_query_data($data_query);
        $res = $this->standard_model->select();

        if(isset($res->id)) {
            $this->response(array('status' => false, 'message'=> "You've already registered!" ));
        } else {
            $insert_data['registered_date'] = date('Y-m-d H:i:s');
            
            $this->standard_model->set_query_data($data_query);
            $id = $this->standard_model->insert_and_id($insert_data);

            $this->response(array('status' => true, 'message'=> "Successfully registered. Thank You!" ));
        }
	}

	public function stats_get($article_id = 0){
		if($article_id ==0) {
			$this->response(array('status' => true, 'message'=> "Invalid request!" ));
		}


		$data['table'] 	= 'articles';
		$data['field']	= 'views';
		$data['condition'] = array(		
			'id' => $article_id
		);		
		$this->standard_model->set_query_data($data);
		$data_results = $this->standard_model->select();

			
		$views = 1;
		if(isset($data_results->views)) {
			$views = $data_results->views+$views;
		}
        
        $set_arr['views'] = $views;
        $set_res = $this->standard_model->update($set_arr);
        echo $set_res;
	}

	private function is_valid_email($email) {
	    return filter_var($email, FILTER_VALIDATE_EMAIL) 
	        && preg_match('/@.+\./', $email);
	}

	private function is_validate_mobile($mobile) {
    	return preg_match('/^[0-9]{10}+$/', $mobile);
	}
}