<?php $this->load->helper('utilities'); ?>
<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet type="text/xsl" href="<?php echo base_url();?>sitemap.xsl" ?>
    <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach ($categories as $category) { ?>	
        <url>
            <loc><?php echo base_url().'sitemap/category/'. $category->cat_slug.'.xml'; ?></loc>
<?php if (isset($category->modified)) { ?>
            <lastmod><?php $datetime = new DateTime($category->modified); echo $datetime->format(DateTime::ATOM);?></lastmod>
<?php } ?>
            <priority>1.0</priority>
        </url>
<?php } ?>       
    </urlset>