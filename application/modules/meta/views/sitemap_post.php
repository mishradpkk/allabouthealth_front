<?php $this->load->helper('utilities'); ?>
<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet type="text/xsl" href="<?php echo base_url();?>sitemap.xsl" ?>
    <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php if($show_index == true) { ?>
        <url>
            <loc><?php echo substr(base_url(), 0, strrpos(base_url(), "/"));?></loc>
            <changefreq>daily</changefreq>
            <priority>1</priority>
        </url>
<?php } foreach($posts as $post) { ?>
        <url>
            <loc><?php echo base_url().$post->slug;?></loc>
            <lastmod><?php $datetime = new DateTime($post->modified); echo $datetime->format(DateTime::ATOM);?></lastmod>
            <changefreq>daily</changefreq>
            <priority>1.0</priority>
<?php if(false){ ?>
            <image:image>
                <image:loc><?php echo base_url().$post->id.'/'.$post->cover_image;?></image:loc>
                <image:title><![CDATA[<?php echo $post->title;?>]]></image:title>
            </image:image>
<?php foreach($post->images as $image) { if(isset($image->media) && strpos($image->media, '&feature') == false) { ?> 
            <image:image>
                <image:loc><?php echo base_url().$post->id.'/'.$image->media;?></image:loc>
<?php if($image->caption != '') { ?>
                <image:caption><![CDATA[<?php echo $image->caption;?>]]></image:caption>
<?php } ?>
            </image:image>            
<?php } }} ?>
        </url>     
<?php } ?>
    </urlset>
    