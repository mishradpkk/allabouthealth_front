<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet type="text/xsl" href="<?php echo base_url();?>sitemap.xsl" ?>
    <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        <url>
            <loc><?php echo base_url(); ?></loc>
            <lastmod><?php echo date("Y-m-d"); ?></lastmod>
            <changefreq>daily</changefreq>
            <priority>1</priority>            
        </url>
        <url>
            <loc><?php echo base_url().'terms-of-service'; ?></loc>
            <lastmod><?php echo date("Y-m-d"); ?></lastmod>            
            <changefreq>weekly</changefreq>
            <priority>0.7</priority>
        </url>        
        <url>
            <loc><?php echo base_url().'privacy-policy'; ?></loc>
            <lastmod><?php echo date("Y-m-d"); ?></lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.7</priority>
        </url>
        <url>
            <loc><?php echo base_url().'about-us'; ?></loc>
            <lastmod><?php echo date("Y-m-d"); ?></lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.7</priority>
        </url>
    </urlset>    