<?xml-stylesheet type="text/xsl" href="<?php echo base_url();?>sitemap.xsl" ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php $index=1; foreach ($lastmod as $mod) { ?>
	<sitemap>
		<loc> <?php echo base_url() .'sitemap/post-sitemap-'.$index.'.xml' ?> </loc>
		<lastmod><?php $datetime = new DateTime($mod->modified); echo $datetime->format(DateTime::ATOM);?></lastmod>
	</sitemap>
<?php $index++; } ?>
	<sitemap>
		<loc> <?php echo base_url() .'category-sitemap.xml' ?> </loc>
		<lastmod><?php echo $datetime->format(DateTime::ATOM);?></lastmod>
	</sitemap>
	<sitemap>
		<loc> <?php echo base_url() .'page-sitemap.xml' ?> </loc>
		<lastmod><?php echo $datetime->format(DateTime::ATOM);?></lastmod>
	</sitemap>		
</sitemapindex>