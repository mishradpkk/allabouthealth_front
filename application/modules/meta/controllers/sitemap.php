<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('xmlrpc');
		$this->load->library('Xml_writer');
		$this->load->library('sitemaps');
		$this->load->library('memcached_library');
		$this->load->model('standard_model');
	}

	public function index() {
		$this->main_sitemap();
	}

	public function main_sitemap($post_map = null) {

		header('Content-type: application/xml; charset=utf-8');		

		if($post_map == null) {

			$total_posts = $this->total_posts();

			if($total_posts > 1000) {
				$count_sm = floor($total_posts/1000)+1;
				
				for ($i=0; $i < $count_sm; $i++) {
					$count_mod = $i*1000;
					$post_data['lastmod'][$i] = $this->get_lastmod($count_mod);
				}
			} else { 
				$post_data['lastmod'][0] = $this->get_lastmod(0);
			}

			$this->load->view('sitemapindex', $post_data);
		} else {

			$offset_count = intval(preg_replace('/[^0-9]+/', '', $post_map), 10);

			$post_data['show_index'] = false;
			if($offset_count == 1) {
				$post_data['show_index'] = true;
			}
			
			if($offset_count <=0)
				$offset_count = 1;
			
			$offset_count = ($offset_count-1)*1000;
			$post_data['posts'] = $this->get_posts($offset_count);

			$this->load->view('sitemap_post', $post_data);	
		}
	}

	public function page_sitemap() {
		header('Content-type: application/xml; charset=utf-8');
		$this->load->view('sitemap_page');	
	}

	public function category_sitemap($cat_slug=null){

		header('Content-type: application/xml; charset=utf-8');

		if ($cat_slug == null) {

			$file_name = "sitemap_categoriy";
			$mem_cache_data = $this->memcached_library->get($file_name);

			if (!$mem_cache_data) {
				$data_cat['table'] = 'categories' ;
		    	$data_cat['field'] = "id,cat_slug";
		    	$data_cat['condition'] = array(
				  'status' => 1
			    );

		    	$this->standard_model->set_query_data($data_cat);
				$cat_data['categories'] = $this->standard_model->select();

				foreach($cat_data['categories'] as $category) {
					$data_date['table'] = 'articles' ;
			    	$data_date['field'] = "date_of_action";
			    	$data_date['condition'] = array(
					  'cat_id' => $category->id,
				    );
				    $data_date['where_in'] = array(
			            'where_in_field' => "status",
			            'where_in_array' => array('active','hidden')
			        );
			        $data_date['limit'] = 1;	        
			        $data_date['order_by'] = array('date_of_action' => 'desc');

			    	$this->standard_model->set_query_data($data_date);
					$modified = $this->standard_model->select();
					if($modified) {
						$category->modified = $modified->date_of_action;				
					}
				}
				$this->memcached_library->add($file_name, $cat_data, SITEMAP_CACHE_LIMIT);			
				$this->load->view('sitemap_category', $cat_data);	
			} else {			
				$this->load->view('sitemap_category', $mem_cache_data);
			}

		}else{

			// $final_cat = explode('-', $cat_slug);

		    	// if (sizeof($final_cat) == 3) {
		    	// 	$cat_slug = $final_cat[0].' & '.$final_cat[1].' & '.$final_cat[2];
		    	// }else if (sizeof($final_cat) == 2) {
		    	// 	$cat_slug = $final_cat[0].' & '.$final_cat[1];
		    	// }else{
		    	// 	$cat_slug = $final_cat[0];
		    	// }

		    	$err['error'] = 'bad arg passed';
		    	if($cat_slug==null) {
		    		echo json_encode($err);
		    		return false;
		    	}
				// main category data
		    	$data_cat['table'] = 'categories' ;
		    	$data_cat['field'] = "id,name, cat_slug";
		    	$data_cat['condition'] = array(
				  'status' => 1,
				  'cat_slug' => $cat_slug
			    );
				
		    	$this->standard_model->set_query_data($data_cat);
				$total_cat = $this->standard_model->select();

				$data['table'] = 'articles' ;
		    	$data['field'] = "id, slug, title, cover_image, date_of_action as modified";
		    	$data['condition'] = array(
		    		'cat_id' => $total_cat->id,
		    		'status' => 'active'
		    	);
			    //$data['limit'] = 1000;
		        //$data['offset'] = $offset;
		        $data['order_by'] = array('date_of_creation' => 'desc');	        

		    	$this->standard_model->set_query_data($data);
		    	$result['posts'] = $this->standard_model->select();
		    	if(is_object($result['posts'])) {
		    		$result['posts'] = array($result['posts']);
		    	}

		    	/*$i=0;
		    	foreach ($result['posts'] as $post) {
					$data_media['table'] = 'media' ;
			    	$data_media['field'] = "id,media,caption";
				    $data_media['condition'] = array(
				    						'image_status' => 'active',
				    						'media_source' => 'other',
				    						'post_id' => $post->post_id
				    					);
				    $this->standard_model->set_query_data($data_media);
		    		$post->images = $this->standard_model->select();
		    	}*/
		    	$result['show_index'] = true;
				$this->load->view('sitemap_post', $result);	
		}		

	}

	public function sitemap_style() {
		$this->load->view('sitemap_style');
	}


	public function total_posts()	{
		$file_name = "sitemap_index";
		$mem_cache_data = $this->memcached_library->get($file_name);

		if (!$mem_cache_data) {
			$data['table'] = 'articles' ;
	    	$data['field'] = "count(id) as total_posts";
	    	$data['condition'] = array(
	    		'status' => 'active'
	    	);
		    $this->standard_model->set_query_data($data);
	    	$result = $this->standard_model->select()->total_posts;
	    	$this->memcached_library->add($file_name, $result, SITEMAP_CACHE_LIMIT);
	    	return $result;
		} else {
			return $mem_cache_data;
		}
	}

	public function get_lastmod($offset=0) {
		$file_name = "sitemap_lastmod_".$offset;
		$mem_cache_data = $this->memcached_library->get($file_name);

		if (!$mem_cache_data) {
			$data['table'] = 'articles' ;
	    	$data['field'] = "date_of_creation as modified";
	    	$data['condition'] = array(
	    		'status' => 'active'
	    	);
		    
	        $data['limit'] = 1000;
	        $data['offset'] = $offset;
	        $data['order_by'] = array('date_of_creation' => 'desc');

	    	$this->standard_model->set_query_data($data);
	    	$result = $this->standard_model->select();
	    	
	    	//$res_len = sizeof($result)-1;
	    	$this->memcached_library->add($file_name, $result[0], SITEMAP_CACHE_LIMIT);	    	
	    	return $result[0];
		} else {
			return $mem_cache_data;
		}
	}

	public function get_posts($offset) {
		$file_name = "sitemap_posts_".$offset;
		$mem_cache_data = $this->memcached_library->get($file_name);

		if (!$mem_cache_data) {
			$data['table'] = 'articles' ;
	    	$data['field'] = "id, slug, articles.status, title, cover_image, date_of_action as modified";
	    	$data['condition'] = array(
	    		'status' => 'active',	    			    		
	    	);
		    
	        $data['limit'] = 1000;
	        $data['offset'] = $offset;
	        $data['order_by'] = array('date_of_creation' => 'desc');	        

	    	$this->standard_model->set_query_data($data);
	    	$result = $this->standard_model->select();

	    	/*foreach ($result as $post) {
				$data_media['table'] = 'media' ;
		    	$data_media['field'] = "id,media,caption";
			    $data_media['condition'] = array(
			    						'status' => 1,
			    						'media_type' => 'other',
			    						'post_id' => $post->id
			    					);
			    $this->standard_model->set_query_data($data_media);
	    		$post->images = $this->standard_model->select();
	    	}*/
	    	$this->memcached_library->add($file_name, $result, SITEMAP_CACHE_LIMIT);	    	
	    	return $result;
		} else {
			return $mem_cache_data;
		}
	}
}
?>
