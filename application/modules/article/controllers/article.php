<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct(){
		parent::__construct();		 
		$this->load->helper('utilities');
		$this->load->model('standard_model');
		$this->load->helper('settings');		
		$this->load->helper('query');
		$this->load->library('memcached_library');
	}

	public function index() {
		redirect('/');
	}

	public function route_to($slug="", $article_slug='') {

		if($slug==''){
			redirect(base_url());
		} else if($article_slug=='') {
			$category = $this->get_category_details($slug);

			if(isset($category->id) || $slug=='popular') {

				// if($slug == 'popular') {
				// 	$this->popular_section(20);
				// } else if($slug == 'latest') {
				// 	$this->latest_section(20);
				// } else {
					$category->slug = $slug;

					// if($article_slug == 'latest' || $article_slug == 'popular') {
						// $this->category_by_section($category, $slug2, 20);
					// } else {
						$this->category_details($category);
					// }
				// }
			} 
			else {
				
			}
		} else {
			$this->article_data($article_slug);
		}

		// echo "<pre>";
		// print_r($category);
		// echo "</pre>";

		// } else {			
		// 	redirect('/');
		// }
	}

	private function get_category_details($slug='') {
		
		$data_query['table'] 	= 'categories';
		$data_query['field']	= 'id,name, about, cover_image, meta_title, meta_description, keywords';
		$data_query['limit']	= 1;
		$data_query['condition'] = array(
			'is_active' => '1',
			'slug' => $slug
		);				
		
		$this->standard_model->set_query_data($data_query);
		$cat_detail = $this->standard_model->select();
		
		
		return $cat_detail;
	}

	public function category_details($category=array()) {
		$latest	= $this->articles("latest", $category->id, 12);
		
        $data['latest_articles'] = $latest['articles'];
        $data['category'] = $category;
		
		$data['main_content']   = "home/category_view";

		$this->load->view('master',$data);
	}

	
	private function articles($story_type="", $cat_id=0, $limit=12) {
		$offset = 0;

		$data_query['table'] 	= 'articles';
		$data_query['field']	= 'articles.id, articles.title, articles.cover_image, articles.slug, short_description, categories.name as category, categories.slug as cat_slug';
		
		$data_query['join']  = array(
			"categories" => "categories.id = articles.cat_id",			
		);

		$data_query['condition'] = array(
		 	'articles.status' => 'active',
		 	'articles.is_deleted' => 0,
		 	'articles.is_promoted !=' => '1',				 	
		); 
		$data_query['order_by'] = array('date_of_action' => 'desc' );	
	
		if($cat_id!=0) {
			$data_query['condition']['articles.cat_id'] = $cat_id;
		}

		$data_query['limit'] = $limit;
    	$data_query['offset'] = $offset;	

    	$this->standard_model->set_query_data($data_query);
		$data_results = $this->standard_model->select();

		if(is_object($data_results)) {
			$data_results = array($data_results);
		}

		if(sizeof($data_results)<1) {
			$data_results = array();	
		}
		$results['articles'] = $data_results;
		$this->load->helper('query');
		$results['total_pages'] = get_total_pages($data_query, $limit);

		return $results;
	}

	public function update_vcount() {
		if($this->input->post('id')) {
			echo $this->input->post('id');
	    	$data['table'] = 'articles';
	    	$data['condition'] = array('id' => $this->input->post('id'));
	    	$data['field'] = 'id,views';
	    	$this->standard_model->set_query_data($data);
	    	$res = $this->standard_model->select();
	    	if ($res->id) {
	    		$count = $res->views;    		
	    		$stats = $this->standard_model->update(array('views' => (intval($count)+1)));
	    		echo $stats;
	    	}			
		}
	}

	public function article_data($slug="", $page_no=1) {
		$data['main_content'] = "article_view";
		$data['load'] = 'static';
		$data['article'] = $this->article_description($slug);

		
		if(!isset($data['article']->cat_name)) {			
			redirect('/');
			exit();
		}

		$image_parameter = 0;
		if($this->input->get('i')) {
    		$image_parameter = $this->input->get('i');
		}

		if($this->input->get('I')) {
    		$image_parameter = $this->input->get('I');
		}

		if($image_parameter > 0) {
			$data['layout'] = 'multiple';

			if($page_no > 1) {
				$data['layout'] = 'multiple_second';
			} 
		} else {
			$data['layout'] = 'single';
		}

		if (isset($data['article']->id)) {
			$data['page_param'] = $page_no;
			
			if($data['layout'] != 'single') {
				$data['article']->total_media = $this->get_total_media_count($data['article']->id);				

				$data['image_param'] = $image_parameter;	
				if( $image_parameter > 0 ) {
		        	$data['total_pages'] = $this->get_total_page_count($data['article']->id, $image_parameter, $data['article']->total_media);
		        }

		        if( $page_no > 0 ) {
		        	$data['page_no'] = $page_no;
		        }else{
		        	$data['page_no'] = 1;
		        }

	        	if ($data['page_no'] != 0) {
	    			$data['media'] = $this->article_media($data['article']->id, $image_parameter, ($image_parameter*($data['page_no']-1)), $data['page_no']);
	    		}

			} else {				
				$data['media'] = $this->article_media($data['article']->id);
			}

			$data['next_article'] = get_next_story($data['article']->id);
			$data['prev_article'] = get_prev_story($data['article']->id);

	        
			$data['related_artiles'] = $this->article_related($data['article']->cat_id, $data['article']->id);
			// $data['related_artiles'] = $this->article_editors_pick($data['article']->cat_id);
			// if($data['layout'] != 'single') {
			// 	$data['article_id'] = $data['article']->id;
			// 	$data['editors_pick_articles']	= you_may_also_like($data['article']->id, $data['article']->id);				
			// }
		}

		// if ($_SERVER['HTTP_HOST'] != 'localhost') {
        	// $this->output->cache(HTML_CACHE_MINUTE);
    	// }		

    	$data['read_next'] = $this->read_next_articles($data['article']->id);

    	$data['rand_anchor'] = rand(1,2);

		$this->load->view('master',$data);
	}

	private function read_next_articles($article_id=0) {
		$data_query['table'] 	= 'articles';
		$data_query['field']	= 'articles.id,title,short_description,articles.cover_image,articles.slug, categories.slug as cat_slug';
		$data_query['condition'] = array(
		 	'articles.status' => 'active',
		 	'articles.is_deleted' => 0,
		 	'articles.id !=' => $article_id,
		); 
		$data_query['join']	= array(
			'categories' => 'articles.cat_id=categories.id'
		);
		$data_query['order_by'] = array('date_of_action' => 'desc' );	
		$data_query['limit'] = 10;
    	$data_query['offset'] = 0;	

    	$this->standard_model->set_query_data($data_query);
		$data_results = $this->standard_model->select();

		if(is_object($data_results)) {
			$data_results = array($data_results);
		}
		return $data_results;
	}
	
	public function get_total_media_count($article_id=0)
	{
		if($article_id==0){
			return 0;
		}

		$data['table'] = 'media' ;
    	$data['field'] = "count(id) as total_media";
    	$data['condition'] 	=  array( 'post_id' => $article_id, 'status' => '1');

    	$this->standard_model->set_query_data($data);
		$result = $this->standard_model->select()->total_media;
		return $result;		
	}
	

	public function get_total_page_count($article_id =-1, $limit = 0, $total_media){
		if($article_id == -1 || !(is_numeric($article_id))) {
        	redirect(base_url());        
        }	        
        
        $page = 0;

        $page = ($total_media/$limit);
                    
        if(!is_int($page)) {
            $page=ceil($page);
        }

    	return $page ;
    }

	public function article_description($slug="") {

		if ($slug == "") {
			return false;
		} 

		$data_query['table'] 	= 'articles';
		
		$data_query['field']	= 'articles.id, articles.title, articles.cat_id, articles.caption, short_description, articles.cover_image, articles.slug, articles.tags, articles.description, date_of_creation, date_of_action, categories.name as cat_name, categories.slug as cat_slug, users.full_name as username, users.full_name as username, users.slug as user_slug';
		
		$data_query['join']	= array(
			'categories' => 'articles.cat_id=categories.id', 
			'users' => 'users.id=articles.user_id', 
		);
		
		$data_query['condition'] = array(		
			'articles.status' => 'active',
			'articles.is_deleted' => 0,
		  	'articles.slug' => $slug
		);
		
	    $data_query['limit'] = 1;
		$this->standard_model->set_query_data($data_query);
		$data_results = $this->standard_model->select();

		return $data_results;		
	}

	public function article_media($article_id=0, $limit=0, $offset=0, $current_page = 0) {

		if ( $article_id <= 0) {
    		return;
    	}

    	$data['table'] 		= 'media';
		$data['field'] 		= 'id,title, image, caption, description, ';
    	$data['condition'] 	=  array( 'article_id' => $article_id, 'status' => '1');
	    $data['order_by']   = array('media_order' => 'ASC');

	    // $data['limit']  = 2;
	    if ($limit >0) {
	    	$data['limit']  = $limit;
    		$data['offset'] = $offset;		    	
	    }

    	$this->standard_model->set_query_data($data);
		$data_result = $this->standard_model->select();

		if(is_array($data_result)) {
			$data_results = $data_result;
		} else {
			$data_results[0] = $data_result;
		}

		return $data_results;		
	}

	public function article_editors_pick($article_id=0) {
		$data['table'] 	= 'articles';
		$data['field']	= 'articles.id,title,articles.cover_image,articles.slug,articles.description,
				date_of_action,categories.name as category,categories.slug as cat_slug';
		$data['join']	= array(
			'categories' => 'articles.cat_id=categories.id'
		);
		$data['condition'] = array(
			'articles.is_editors_pick' => 1,
			'articles.id !=' => $article_id,
			'articles.status' => 'active',
			'articles.is_deleted' => 'active',
		);
		
		$data['limit'] = 5;
		$this->standard_model->set_query_data($data);
		
		$data_result = $this->standard_model->select();
		
		if(is_array($data_result)){
			$data_results = $data_result;
		} else {
			$data_results[0] = $data_result;
		}
		
		return $data_results;	
	}

	public function article_related($cat_id=0, $article_id=0)	{
		$data['table'] 	= 'articles';
		$data['field']	= 'articles.id,title,articles.cover_image,articles.slug,articles.description,
				date_of_action,categories.name as category,categories.slug as cat_slug';
		$data['join']	= array(
			'categories' => 'articles.cat_id=categories.id'
		);
		$data['condition'] = array(
			'articles.cat_id' => $cat_id,
			// 'articles.is_promoted' => 1,
			'articles.id !=' => $article_id,
			'articles.status' => 'active',	
			'articles.is_deleted' => 0,
		);
		
		$data['limit'] = 5;
		$this->standard_model->set_query_data($data);
		
		$data_result = $this->standard_model->select();
		
		if(is_array($data_result)){
			$data_results = $data_result;
		} else {
			$data_results[0] = $data_result;
		}
		
		return $data_results;		
	}

	

}
