<?php 
$this->load->helper('utilities');

$count = 0; foreach ($media as $res) { if($res) { ?>
    <div class="css-0">
        <h2>
            <div><a name="What-happens-when-we-experience-stressful-events?"><?php echo $res->title; ?></a></div>
        </h2>
        <?php if(isset($res->image) && $res->image!=''){ ?>
        <div>
            <figure class="css-z8zrn2">
                <img src="<?php echo CDN_IMAGE_URL . $res->id . '/'. $res->image;?>" />
                <figcaption class="css-bx4197" style="padding: 0px;"><?php echo $res->caption;?></figcaption>
            </figure>
        </div>
        <?php } ?>
        <?php echo $res->description; ?>
    </div>
<?php } $count++; } ?>