<div id="editorspick-<?php echo $article_id; ?>" class="editorspick-section mb-2 pt-4">
    <div class="section-head align-middle text-center">
        <span class="side-line">
            <svg width="61" height="6" viewBox="0 0 61 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M3 0H61L58 6H0L3 0Z" fill="#ED6A5A"/>
            </svg>
        </span>
    
        <span class="head-title">Editor’s Pick</span>
    
        <span class="side-line">
            <svg width="61" height="6" viewBox="0 0 61 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M3 0H61L58 6H0L3 0Z" fill="#ED6A5A"/>
            </svg>
        </span> 
    </div>

    <div class="editors_pick_articles row mt-4">
        <?php foreach ($editors_pick_articles as $article) { ?>
        <div class="col-12 col-md-4 article mb-3">
            <article class="h-100 bg-white">
                <div class="">                    
                    <div class="cover-wrapper">
                        <a href="<?php echo base_url() . $article->slug; ?>">
                            <?php if(file_exists(UPLOAD_PATH.$article->id . '/' . $article->cover_image.'.webp')) { ?>
                                <img class="d-block w-100" src="<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>.webp" onerror="this.src='<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>';this.onerror='';" alt="<?php echo $article->title; ?>">
                            <?php } else { ?>                                
                                <img class="d-block w-100" src="<?php echo IMAGE_PATH . $article->id . '/' . $article->cover_image; ?>" alt="<?php echo $article->title; ?>">
                            <?php } ?>
                            <div class="category-tag small">
                                <a href="<?php echo base_url() . $article->cat_slug; ?>"><?php echo $article->category; ?></a>
                            </div>
                        </a>
                    </div>                    
                    
                    <div class="body-wrapper">
                        <a href="<?php echo base_url() . $article->slug; ?>">
                            <h3 class="title text-left"><?php echo $article->title; ?></h3>
                        </a>                        
                    </div>                    
                </div>
            </article>
        </div>
        <?php } ?> 

        
    </div>          
</div>