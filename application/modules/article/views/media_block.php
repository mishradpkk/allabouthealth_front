<?php $this->load->helper('utilities'); ?>
<div class="alm-listing scrollspy alm-ajax" data-media="<?php echo $index;?>">
    <div class="alm-nextpage post-<?php echo $res->post_id; ?>">
        <h3><?php if($res->caption) { echo normalize_str($res->caption); }?></h3>
        <figure class="wp-block-image alignfull size-large">
            <img
                src="<?php echo IMAGE_PATH.$res->post_id.'/'.$res->media; ?>"
                alt=""                                                        
            />
            <figcaption><a href="<?php echo $res->source;?>">via</a></figcaption>
        </figure>
        <div class="code-block code-block-1" style="margin: 8px 0; clear: both;">
            <div id="teadsbelowimage"></div>
        </div>

        <?php //if($index%2!=0 && $index>0) { ?>
            <div class="adboxtext"><span>ADVERTISEMENT</span></div>
            <div class="ad-container">
                <div>
                <?php if($this->input->get('camp') && $this->input->get('camp') == 'tvs') { ?>
                    <div class="clearfix adBlock" id="ads_<?php echo $res->post_id .'_'.$res->id.'_block1'; ?>"></div>
                <?php } else { ?>
                    <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Banner'></div>
                <?php } ?>
                </div>
            </div>
        <?php //} ?>

        <div style="margin-top:8px;">
        <?php if(isset($res->description)) { echo normalize_str($res->description); }?>
        </div>

        <div class="adboxtext"><span>ADVERTISEMENT</span></div>
        <div class="ad-container">
            <div>
            <?php //if($index%2!=0) { ?>
                
                <?php if($this->input->get('camp') && $this->input->get('camp') == 'tvs') { ?>
                    <div class="clearfix adBlock" id="ads_<?php echo $res->post_id .'_'.$res->id .'_block2'; ?>"></div>
                <?php } else { ?>
                    <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Banner1'></div>
                <?php } ?>

            <?php //} else { ?>
                
                <?php //if($this->input->get('camp') && $this->input->get('camp') == 'tvs') { ?>
                    <!-- <div class="clearfix adBlock" id="ads_<?php echo $res->post_id .'_'.$res->id .'_block1'; ?>"></div> -->
                <?php //} else { ?>
                    <!-- <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/THE_VIRAL_STREET_Banner'></div> -->
                <?php //} ?>

            <?php //} ?>
            </div>
            
        </div>
        <!--<div class="belowadline"><span></span></div>-->
    </div>
</div>