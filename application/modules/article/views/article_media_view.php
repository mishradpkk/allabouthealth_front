<?php 
$this->load->helper('ads');
$this->load->helper('utilities');
// $ads = page_ads($page_param);

$count = 0; foreach ($media as $res) { if($res) { ?>

<div class="alm-listing scrollspy alm-ajax" data-media="<?php echo $count;?>">
    <div class="alm-nextpage post-<?php echo $res->post_id; ?>">
        <h3><?php if($res->caption) { echo normalize_str($res->caption); }?></h3>
        <figure class="wp-block-image alignfull size-large">
            <img
                src="<?php echo IMAGE_PATH.$res->post_id.'/'.$res->media; ?>"
                alt=""                                                        
            />
            <figcaption><a href="<?php echo $res->source;?>">via</a></figcaption>
        </figure>
        <?php if($page_param>=2){ ?>
        <!-- ad code start -->
        <div class="adboxtext"><span>ADVERTISEMENT</span></div>
        <div class="ad-container">
            <div>
                <?php if($count ==0) { ?>
                    <!-- ad1>>>>>>>>>>>>>>>> -->
                    <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad1'></div>
                <?php } else if($count == 1) { ?>
                    <!-- ad3>>>>>>>>>>>>>>>> -->
                    <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/TVS_Ad3'></div>
                <?php } ?>
            </div>
        </div>
        <!-- ad code end -->
        <?php } ?>
        <?php if(isset($res->description)) { echo normalize_str($res->description); }?>

        <div class="adboxtext"><span>ADVERTISEMENT</span></div>
        <div class="ad-container">
            <div>
                <?php if($layout == 'multiple_second') { ?>             
                    <?php if($count ==0) { ?>
                        <!-- ad2>>>>>>>>>>>>>>>> -->
                        <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad2'></div>
                    <?php } else if($count == 1) { ?>
                        <!-- ad4>>>>>>>>>>>>>>>> -->
                        <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad4'></div>
                    <?php } ?>
                <?php } else { ?>
                    <?php if($count ==0) { ?>
                        <!-- ad2>>>>>>>>>>>>>>> -->
                        <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/TVS_Banner_Ad2'></div>
                    <?php } else if($count == 1) { ?>
                        <!-- ad3>>>>>>>>>>>>>>> -->
                        <div data-aaad='true' data-aa-adunit='/21814398523/THE_VIRAL_STREET/TVS_Ad3'></div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>        
    </div>
</div>

<?php } $count++; } ?>