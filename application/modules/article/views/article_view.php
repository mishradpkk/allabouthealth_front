<?php 
    $date_created = strtotime($article->date_of_creation);
    $date_created = date('F d, Y', $date_created);
?>
<div class="css-stl7tm">
    <div class="css-13sft9o">
        <div class="css-z468a2">
            <a class="css-1dnnbrt" target="_blank" rel="noopener noreferrer"><h4 class="css-911n6p"><?php echo $article->cat_name;?></h4></a>
            <h1 class="css-1jisqi6"><?php echo $article->title;?></h1>
            <section class="css-jy1umg">
                <div>
                    <span>Written by <a class="css-zocu4s" href="<?php echo base_url() . 'authors/'. $article->user_slug; ?>"><?php echo $article->username; ?></a></span><span> on <?php echo $date_created; ?></span>
                </div>
            </section>
            <div>
                <article class="article-body css-d2znx6 undefined">
                    <div class="css-1u22pos">
                        <div><p class="css-1rnzyga"><?php echo $article->short_description;?></p></div>
                        <div>
                            <figure class="css-z8zrn2">
                                <img src="<?php echo CDN_IMAGE_URL . $article->id . '/'. $article->cover_image;?>" />
                                <figcaption class="css-bx4197" style="padding: 0px;"><?php echo $article->caption;?></figcaption>
                            </figure>
                        </div>
                        <?php echo $article->description; ?>
                    </div>
                    
                    <?php $this->load->view('media_view'); ?>                
                </article>
            </div>
            
            <div>
                <form novalidate="">
                    <div>
                        <section class="css-11k39sg">
                            <div class="css-1x6oboy"></div>
                            <div class="css-rp3d6">
                                <div class="css-fwihi8">HEALTHLINE NEWSLETTER</div>
                                <div class="css-kp58b2">
                                    <div class="css-1dtbtvx">Get our twice weekly wellness email</div>
                                    <p class="css-1cr3nkl">To inspire you to exercise and eat well, we’ll send you our top health tips and stories, plus must-read news.</p>
                                </div>
                                <div class="css-8nu43k">
                                    <div>
                                        <div class="css-12wslxx">
                                            <div class="css-1c6pjc3">
                                                <div class="css-1rr4qq7">
                                                    <div class="css-qddt1o">Enter your email</div>
                                                    <input type="email" placeholder="Enter your email" name="email" aria-label="Enter your email" class="css-12ffvwx" value="" />
                                                </div>
                                            </div>
                                            <button class="css-1l30t0b" type="submit">
                                                <span class="css-14ktbsh"><span class="css-1huyk6v">SIGN UP NOW</span></span>
                                            </button>
                                        </div>
                                        <p class="css-nz9v2z">Your <a class="css-1pule1j" target="_blank" rel="noopener noreferrer" href="/privacy-policy">privacy</a> is important to us</p>
                                    </div>
                                </div>
                            </div>
                            <div class="css-1tuviqo"></div>
                        </section>
                    </div>
                </form>
            </div>


           <div class="css-pps27j">
                <div class="css-qtpids">
                    <div class="css-4wvzuy">
                        <!-- <div class="button-wrapper print"><a class="css-yvmi2y print button icon icon-hl-print" target="_blank" title="Print this page"></a></div> -->
                        <!-- <div class="button-wrapper email"><a class="css-yvmi2y email button icon icon-hl-email" target="_blank" title="Email this page"></a></div> -->
                        <div class="button-wrapper facebook">
                            <a
                                class="css-yvmi2y facebook button icon icon-hl-facebook"
                                href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(base_url(). $article->cat_slug.'/'.$article->slug); ?>"
                                rel="noopener noreferrer nofollow"
                                target="_blank"
                                title="Share on Facebook"
                            ></a>
                        </div>
                        <div class="button-wrapper twitter">
                            <a
                                class="css-yvmi2y twitter button icon icon-hl-twitter"
                                href="https://twitter.com/intent/tweet?via=allabouthealth&amp;text=<?php echo urlencode($article->title); ?>&amp;url=<?php echo urlencode(base_url(). $article->cat_slug.'/'.$article->slug); ?>"
                                rel="noopener noreferrer nofollow"
                                target="_blank"
                                title="Share on Twitter"
                            ></a>
                        </div>
                        <div class="button-wrapper pinterest">
                            <a
                                class="css-yvmi2y pinterest button icon icon-hl-pinterest"
                                href="https://www.pinterest.com/pin/create/button/?media=[object Object]&amp;description=<?php echo urlencode($article->title); ?>&amp;url=<?php echo urlencode(base_url(). $article->cat_slug.'/'.$article->slug); ?>"
                                rel="noopener noreferrer nofollow"
                                target="_blank"
                                title="Share on Pinterest"
                                data-pin-custom="true"
                                data-pin-do="buttonPin"
                                data-pin-href="https://www.pinterest.com/pin/create/button/?media=[object Object]&amp;description=<?php echo urlencode($article->title); ?>&amp;url=<?php echo urlencode(base_url(). $article->cat_slug.'/'.$article->slug); ?>"
                                media="[object Object]"
                            ></a>
                        </div>
                        <div class="button-wrapper reddit">
                            <a
                                class="css-yvmi2y reddit button icon icon-hl-reddit"
                                href="https://www.reddit.com/submit?url=<?php echo urlencode(base_url(). $article->cat_slug.'/'.$article->slug); ?>"
                                rel="noopener noreferrer nofollow"
                                target="_blank"
                                title="Share on Reddit"
                            ></a>
                        </div>
                    </div>
                    <div class="css-1h23hnl"><a class="css-1hh5k90">FEEDBACK:</a><a class="css-yvmi2y icon icon-hl-frown"></a><a class="css-yvmi2y icon icon-hl-smile"></a></div>
                </div>
            </div>

            <hr>
        </div>
        <section class="css-11iztoc">
            <div class="css-1eq4juu">
                <div class="css-npv6dx">
                    <section class="css-lizeih">
                        <div>
                            <span>Written by <a class="css-zocu4s" href="<?php echo base_url() . 'authors/'. $article->user_slug; ?>"><?php echo $article->username; ?></a></span><span> on <?php echo $date_created;?></span>
                        </div>
                    </section>
                </div>
            </div>
            
            <div class="css-1y1s420" data-empty="true">
                <div style="background-color: #ccc; min-height: 250px; width: 300px;">
                    
                </div>

            </div>
            
            
            <div class="css-1geekmd">
                <h2 class="css-11kr1sc">related stories</h2>
                <ul class="css-17mrx6g">
                    <?php foreach ($related_artiles as $related) { ?>
                    <li class="css-qt919d">
                        <a class="css-o0bb22" href="<?php echo base_url().$related->category.'/'.$related->slug; ?>">
                            <img src="<?php echo CDN_IMAGE_URL . $related->id . '/'. $related->cover_image;?>" alt="" class="css-1e3jfij" />
                        </a>
                        <div class="css-bufxhs">
                            <a class="css-1934zwx" href="<?php echo base_url().$related->category.'/'.$related->slug; ?>">
                                <?php echo $related->title; ?>
                            </a>
                        </div>
                    </li>
                    <?php } ?>                    
                </ul>
            </div>

            <div class="css-h9jo37" data-dynamic-ads="true">
                <div class="css-c9zd8s">
                    <div class="css-12efcmn">
                        <div class="css-ah1nyu">
                            <div class="css-1hjaa0k"></div>
                            <div id="sticky6__slot" data-adbridg-ad-class="sticky6" data-ad="true" class="css-2huc28 up-show" data-guard-element="true" data-google-query-id="CKu7vt7B9O4CFclZKwodL1sMBA">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="css-c9zd8s">
                    <div class="css-12efcmn">
                        <div class="css-ah1nyu">
                            <div class="css-1hjaa0k"></div>
                            <div id="sticky6__slot" data-adbridg-ad-class="sticky6" data-ad="true" class="css-2huc28 up-show" data-guard-element="true" data-google-query-id="CKu7vt7B9O4CFclZKwodL1sMBA">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="css-c9zd8s">
                    <div class="css-12efcmn">
                        <div class="css-ah1nyu">
                            <div class="css-1hjaa0k"></div>
                            <div id="sticky6__slot" data-adbridg-ad-class="sticky6" data-ad="true" class="css-2huc28 up-show" data-guard-element="true" data-google-query-id="CKu7vt7B9O4CFclZKwodL1sMBA">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="css-c9zd8s">
                    <div class="css-12efcmn">
                        <div class="css-ah1nyu">
                            <div class="css-1hjaa0k"></div>
                            <div id="sticky6__slot" data-adbridg-ad-class="sticky6" data-ad="true" class="css-2huc28 up-show" data-guard-element="true" data-google-query-id="CKu7vt7B9O4CFclZKwodL1sMBA">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="css-c9zd8s">
                    <div class="css-12efcmn">
                        <div class="css-ah1nyu">
                            <div class="css-1hjaa0k"></div>
                            <div id="sticky6__slot" data-adbridg-ad-class="sticky6" data-ad="true" class="css-2huc28 up-show" data-guard-element="true" data-google-query-id="CKu7vt7B9O4CFclZKwodL1sMBA">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="css-c9zd8s">
                    <div class="css-12efcmn">
                        <div class="css-ah1nyu">
                            <div class="css-1hjaa0k"></div>
                            <div id="sticky7__slot" data-adbridg-ad-class="sticky7" data-ad="true" class="css-2huc28 up-show" data-guard-element="true" data-google-query-id="CKy7vt7B9O4CFclZKwodL1sMBA">
                                
                            </div>
                        </div>
                        <div class="css-1kzh8va">
                            <div class="css-2gi6go">
                                <h2 class="css-10uv1v6">Was this article helpful?</h2>
                                <div class="css-16zlt9i"><a class="css-1dswjw8">Yes</a><a class="css-1dswjw8">No</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <object type="text/html" data="about:blank" class="css-gji99s"></object>
            </div>

        </section>
    </div>
</div>
<div>
    <div id="read-next" class="css-128bpl6">
        <div class="css-stl7tm">
            <div class="css-1h1rean">
                <div class="css-z468a2"><h3 class="css-4yrxt9">Read this next</h3></div>
                <section class="css-82r5pg"></section>
            </div>
        </div>
        <ul class="css-1l95nvm">
            <?php $index=1; foreach ($read_next as $next) { ?>
            <li class="css-12weawj">
                <div class="css-stl7tm">
                    <div class="css-1h1rean">
                        <div class="css-1r7frvz">
                            <div class="css-lm0pdr">
                                <figure class="css-6b9hi2">
                                    <a class="css-yvmi2y" href="<?php echo base_url().$next->cat_slug.'/'.$next->slug; ?>">
                                        <img style="height: 150px; object-fit: cover;"
                                            src="<?php echo CDN_IMAGE_URL . $next->id . '/'. $next->cover_image;?>"
                                            class="css-1o4i59"
                                        />
                                    </a>
                                </figure>
                                <div class="css-1tmarpy">
                                    <a class="css-jtcg31" href="<?php echo base_url().$next->cat_slug.'/'.$next->slug; ?>">
                                        <?php echo $next->title; ?>
                                    </a>
                                    <p class="css-196kqcw">
                                        <a class="css-2fdibo" href="<?php echo base_url().$next->cat_slug.'/'.$next->slug; ?>" >
                                            <?php echo $next->short_description; ?>
                                        </a>
                                    </p>
                                    <a class="css-0 css-14t6971" href="<?php echo base_url().$next->cat_slug.'/'.$next->slug; ?>" >
                                        READ MORE<span class="css-15xqzls icon-hl-arrow-right css-0"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <section class="css-1n288wl">
                            <?php if($index%2 !=0) { ?>
                            <div class="css-vhteyv" data-empty="true">

                            </div>
                            <?php } ?>
                        </section>
                    </div>
                </div>
            </li>
            <?php $index++; } ?>
        </ul>
    </div>
</div>
